import React from 'react'
import { connect } from 'react-redux'

import ScreenInit from './components/ScreenInit'
import ScreenAttract from './components/ScreenAttract'
import ScreenVideo from './components/ScreenVideo'
import ScreenPostcardDetails from './components/ScreenPostcardDetails'
import ScreenAdmin from './components/ScreenAdmin'
import actions from './actions'
import Utils from './services/utils'

class App extends React.Component{
  state = {}
  constructor(props){
    super(props)
    setInterval(()=>{
        
      if ( this.props.screenKey === "Init" ) return

        const howLong = this.props.nextTimeout - Utils.now()
        if ( howLong<0 ){
            if ( this.props.screenKey === "Attract" ||  this.props.screenKey === "Admin"){
                this.props.dispatch(actions.humanActivity())
            }else{
                this.props.dispatch(actions.setScreen('Attract'))
            }
        }
    },1000); 
  }

  keepAlive = ()=>{
    this.props.dispatch(actions.humanActivity())
  }

  getComponent(key){
    const screens = {Init: <ScreenInit preferences={this.props.preferences}/>,
                    Attract: <ScreenAttract />, 
                     Video: <ScreenVideo/>,
                     PostcardDetails: <ScreenPostcardDetails/>,
                     Admin: <ScreenAdmin/>,
                    }
    return screens[key]
  }
  componentWillMount(){
    this.props.dispatch(actions.gotPreferences(this.props.incomingPrefs))
    if ( document.location.href.indexOf('super') !== -1 ){
      this.props.dispatch(actions.goSuperUser())
    }
  }
  render(){
    return(
        !this.props.currentError ? 
        <div onMouseMove={this.keepAlive} >
          {this.getComponent(this.props.screenKey)}
        </div>
        :
        <div>{this.props.currentError}</div>
    )
  }
}
 
export default connect(Utils.mapStateToProps)(App)