const labels = {

    SCREEN_ATTRACT:{
        title: "Get Involved!",
        button_video: "Leave your Video Comment",
        button_postcard: "Send a Virtual Postcard",
    },

    SCREEN_POSTCARD: {
        title: "Send a Virtual Postcard!",
        button_add_snapshot: "Add a Snapshot!",
        prompt_smile: "Smile!",
        button_cancel: "Cancel",
        button_retake: "Retake",
        button_accept: "Accept",
        button_retake_snapshot:"Retake Snapshot",
        label_friends_name: "Your Friend's Name:",
        default_friends_name: "Your Friend's Name",
        label_your_name: "Your Name:",
        default_your_name: "Your Name",
        label_friends_email: "Your Friend's Email:",
        label_friends_email_warning: "Your Friend's Valid Email",
        prompt_friends_email: "Your Friend's Email",
        button_submit: "Send your Postcard!",
        button_back: "Back",
        dialog_busy: "Saving1...",
        dialog_complete: "Success1",
        postcard_heading: "Greetings from the John Heinz<br/>National Wildlife Refuge in Philadelphia, PA.",
    },

    SCREEN_VIDEO:{
        title: "Record a video comment",
        waiver: "By submitting your recorded video comments, you agree to allow your comments to be viewed by others, including the general visiting public.",
        button_record: "Record",
        button_rerecord: "Re-Record",
        button_cancel: "Cancel",
        button_stop: "Stop",
        button_preview: "Preview",
        label_name:"Name:",
        prompt_name:"Your Name",
        label_email:"Email:",
        prompt_email:"Your Email",
        button_back: "Back",
        countdown_prefix: "Starting in ",
        countdown_end_prefix: "Ending in ",
        dialog_busy: "Saving...",
        dialog_complete: "Success",
    }
}

export default labels