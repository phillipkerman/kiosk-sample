
import Utils from './services/utils'

// actions
export const ANOTHER_CONST = 'ACTION_MAKE_CLICKABLE'
export const DO_TICK = 'ACTION_SETUP_STARTED'
export const ACTION_SET_CAROUSEL_REF = 'ACTION_SET_CAROUSEL_REF'
export const ACTION_VIDEO_ENDED = 'ACTION_VIDEO_ENDED'
export const ACTION_VIDEO_PLAYING = 'ACTION_VIDEO_PLAYING'
export const ACTION_SET_SCREEN = 'ACTION_SET_SCREEN'
export const ACTION_HUMAN_ACTIVITY = 'ACTION_HUMAN_ACTIVITY'
export const ACTION_SET_POSTCARD_FRONT = 'ACTION_SET_POSTCARD_FRONT'
export const ACTION_SET_SNAPSHOT = 'ACTION_SET_SNAPSHOT'

export const ACTION_POSTCARD_SAVING = 'ACTION_SAVING_POSTCARD'
export const ACTION_POSTCARD_SAVED = 'ACTION_POSTCARD_SAVED'
export const ACTION_POSTCARD_ERROR = 'ACTION_POSTCARD_ERROR'

export const ACTION_STATUS_VIDEO_SAVING = 'ACTION_SAVING_VIDEO'
export const ACTION_VIDEO_SAVED = 'ACTION_VIDEO_SAVED'
export const ACTION_VIDEO_ERROR = 'ACTION_VIDEO_ERROR'
export const ACTION_STALE_THE_DIALOG = 'ACTION_STALE_THE_DIALOG'

export const ACTION_SET_ADMIN_ROW = 'ACTION_SET_ADMIN_ROW'
export const ACTION_SET_ADMIN_ROW_NEXT = 'ACTION_SET_ADMIN_ROW_NEXT'
export const ACTION_ADD_TO_PENDING = 'ACTION_ADD_TO_PENDING'
export const ACTION_TAKE_OUT_OF_PENDING = 'ACTION_TAKE_OUT_OF_PENDING'
export const ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE = 'ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE'

export const ACTION_TAKE_OUT_OF_PENDING_UNTRASH = 'ACTION_TAKE_OUT_OF_PENDING_UNTRASH'

export const ACTION_SERVER_ERROR = 'ACTION_SERVER_ERROR'
export const ACTION_GETTING_VIDEOS = 'ACTION_GETTING_VIDEOS'
export const ACTION_GOT_VIDEOS = 'ACTION_GOT_VIDEOS'
export const ACTION_GOT_DELETED_VIDEOS = 'ACTION_GOT_DELETED_VIDEOS'
export const ACTION_ERROR_GETTING_VIDEOS = 'ACTION_ERROR_GETTING_VIDEOS'
export const ACTION_ADD_SELECTED_VIDEOS = 'ACTION_ADD_SELECTED_VIDEOS'
export const ACTION_REM_SELECTED_VIDEOS = 'ACTION_REM_SELECTED_VIDEOS'
export const ACTION_OPEN_VIDEO_PLAYER = 'ACTION_OPEN_VIDEO_PLAYER'
export const ACTION_SET_ADMIN_TAB = 'ACTION_SET_ADMIN_TAB'
export const ACTION_SET_PREF = 'ACTION_SET_PREF'
export const ACTION_GOT_PREFERENCES = 'ACTION_GOT_PREFERENCES'
export const ACTION_ERROR_GETTING_PREFERENCES = 'ACTION_ERROR_GETTING_PREFERENCES'
export const ACTION_TURN_ON_SUPERUSER = 'ACTION_TURN_ON_SUPERUSER'

export const PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN = 'PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN'
export const PREF_COUNTDOWN_BEFORE_VIDEO_STARTS = 'PREF_COUNTDOWN_BEFORE_VIDEO_STARTS'
export const PREF_MAX_VIDEO_DURATION = 'PREF_MAX_VIDEO_DURATION'
export const PREF_START_WARNING_AT = 'PREF_START_WARNING_AT'
export const PREF_INACTIVITY_TIMEOUT = 'PREF_INACTIVITY_TIMEOUT'
export const from_email = 'from_email'
export const from_password = 'from_password'
export const from_service = 'from_service'
export const PREF_POSTCARD_COUNT = 'PREF_POSTCARD_COUNT'


const actions =  {

  gotPreferences: (obj) =>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_GOT_PREFERENCES, data: obj})
    }
  },

  pollPreferences: (pairs = {})=>{
    return (dispatch, getState) =>{
     
      Utils.setPreferences(pairs)
      .then( result => {
              result.json().then( json => {
                //console.log("inside poll Preferences", json)
                dispatch({type: ACTION_GOT_PREFERENCES, data: json})
              })
      })
      .catch( err => {
          dispatch({type: ACTION_ERROR_GETTING_PREFERENCES, data: String(err)})
      })
    }
  },

  setPreferences: (pairs)=>{
      return (dispatch, getState) =>{
     
        Utils.setPreferences(pairs)
        .then( result => {
                result.json().then( json => {
                  //console.log("inside setPreferences", json)
                  dispatch({type: ACTION_GOT_PREFERENCES, data: json})
                })
        })
        .catch( err => {
            dispatch({type: ACTION_ERROR_GETTING_PREFERENCES, data: String(err)})
        })
      }
    },
  
  saveSnapshot: (pictureData)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_SET_SNAPSHOT, data: pictureData})
    }
  },

  setPostcardFront: (toWhat)=>{
   // console.log(toWhat)
    return (dispatch, getState)=>{
      dispatch({type: ACTION_SET_POSTCARD_FRONT, data: toWhat})
    }
  },

  goSuperUser: ()=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_TURN_ON_SUPERUSER})
    }
  },

  humanActivity: () =>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_HUMAN_ACTIVITY})
    }
  },

  setScreen: (screenString) =>{
    return (dispatch, getState) =>{
      dispatch({type: ACTION_SET_SCREEN, data: screenString})
    }
  },

  refreshVideoList: (andGoToScreen)=>{
    return (dispatch, getState) =>{
     const id = setTimeout(()=>{
        dispatch({type: ACTION_ERROR_GETTING_VIDEOS, data: "timeout"})
      }, 20000)
      Utils.getAllVideos()
      .then( result => {
          clearTimeout(id)
              result.json()
              .then( json => {
                
                dispatch({type: ACTION_GOT_VIDEOS, data: json})

                Utils.getDeletedVideos()
                .then( result2 =>{
                  result2.json()
                  .then ( json2 => {
                    dispatch({type: ACTION_GOT_DELETED_VIDEOS, data: json2})
                  })

                  if ( andGoToScreen ){
                    dispatch({type: ACTION_SET_SCREEN, data: andGoToScreen})
                  }

                })

              })
      })
      .catch( err => {
          clearTimeout(id)
          console.log("ID "+id)
          dispatch({type: ACTION_ERROR_GETTING_VIDEOS, data: String(err)})
      })
    }
  },

  setCarouselItemRef: (ref) =>{
    return (dispatch, getState)=>{
        dispatch({type: ACTION_SET_CAROUSEL_REF, data: ref})
    }
  },

  tick: (v) => {
    return (dispatch, getState) =>{
      dispatch({type: DO_TICK, data: getState().ticks+1})
    }
  },

  carouselVideoEnded: () => {
    return (dispatch, getState) =>{
       dispatch({type: ACTION_VIDEO_ENDED})
    }
  },

  carouselVideoStarted: ()=>{
    return (dispatch, getState) =>{
      dispatch({type: ACTION_VIDEO_PLAYING})
   }
  },

  startPostcard: ()=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_POSTCARD_SAVING})
    }
  },

  sendPostcard: (bin, to_email, to_name, from_name,from_service, from_email, from_password, subject) =>{
    
    return (dispatch, getState)=>{
      Utils.sendPostcard(bin, to_email, to_name, from_name, from_service, from_email, from_password, subject)
      .then( result => (
        dispatch({type: ACTION_POSTCARD_SAVED})
      ))
      .catch( err => (
         dispatch({type: ACTION_POSTCARD_ERROR, data: err})
      ))

    }
  },  

  trashVideos: (idlist) =>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_SET_ADMIN_ROW, data: -1})
    
      Utils.deleteVideos(idlist)
      .then( result => {
        result.json().then( json => {
          dispatch({type: ACTION_GOT_VIDEOS, data: json})
        })
      })
      .catch( err => (
         dispatch({type: ACTION_SERVER_ERROR, data: err})
      ))
    }
  },

  archiveVideos: (idlist, andAutoAdvance=false)=>{
    return (dispatch, getState)=>{
      
      dispatch({type: ACTION_ADD_TO_PENDING, data: idlist})

      Utils.archiveVideos(idlist)
      .then( result => {
        result.json().then( json => {
          if ( andAutoAdvance ){
            dispatch({type: ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE, data: {idlist:idlist, videos:json}})
          }else{
            dispatch({type: ACTION_TAKE_OUT_OF_PENDING, data: {idlist:idlist, videos:json}})
          }
        })
      })
      .catch( err => (
         dispatch({type: ACTION_SERVER_ERROR, data: err})
      ))

    }
  },

  undeleteVideos: (idlist) =>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_ADD_TO_PENDING, data: idlist})

      Utils.undeleteVideos(idlist)
      .then( result => {
        result.json().then( json => {
         dispatch({type: ACTION_TAKE_OUT_OF_PENDING_UNTRASH, data: {trashids:idlist, deletedvideos:json}}) 
        })
      })
      .catch( err => (
        dispatch({type: ACTION_SERVER_ERROR, data: err})
      ))

    }
  },

  permanentlyDelete: (idlist)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_ADD_TO_PENDING, data: idlist})
    
      Utils.permanentlyDelete(idlist)
      .then( result => {
        result.json().then( json => {
         dispatch({type: ACTION_TAKE_OUT_OF_PENDING_UNTRASH, data: {trashids:idlist, deletedvideos:json}}) 
        })
      })
      .catch( err => (
        dispatch({type: ACTION_SERVER_ERROR, data: err})
      ))
    }
  },

  putVideosInLoop: (idlist, way, andAutoAdvance=false )=>{
    return (dispatch, getState)=>{
      
      dispatch({type: ACTION_ADD_TO_PENDING, data: idlist})

      Utils.includeInLoop(idlist, way)
      .then( result => {
        result.json().then( json => {
          if( andAutoAdvance ){
            dispatch({type: ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE, data: {idlist:idlist, videos:json}})
          }else{
            dispatch({type: ACTION_TAKE_OUT_OF_PENDING, data: {idlist:idlist, videos:json}})
          }
        })
      })
      .catch( err => (
         dispatch({type: ACTION_SERVER_ERROR, data: err})
      ))

    }
  },

  addSelectedVideos: (list)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_ADD_SELECTED_VIDEOS, data: list})
    }
  },

  removeSelectedVideos: (list)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_REM_SELECTED_VIDEOS, data: list})
    }
  },

  clickVideoInRow: (info)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_OPEN_VIDEO_PLAYER, data: info})
    }
  },

  setAdminRow: (idx)=>{
    return (dispatch, getState)=>{
        dispatch({type: ACTION_SET_ADMIN_ROW, data: idx})
    }
  },

  setFilterTab: (tab)=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_SET_ADMIN_TAB, data: tab})
    }
  },

  nextAdminRow: ()=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_SET_ADMIN_ROW_NEXT})
    }
  },

  saveVideo: (bin, details) =>{
   
    return (dispatch, getState)=>{
      
      dispatch({type: ACTION_STATUS_VIDEO_SAVING})

      Utils.saveVideo(bin, details)
      .then( result => (
        dispatch({type: ACTION_VIDEO_SAVED, data: result})
      ))
      .catch( err => (
         dispatch({type: ACTION_VIDEO_ERROR, data: err})
      ))

    }
  },

  dialogGettingStale: ()=>{
    return (dispatch, getState)=>{
      dispatch({type: ACTION_STALE_THE_DIALOG})
    }
  },

}

export default actions
