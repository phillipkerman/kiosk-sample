import React from 'react';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import './css/style.css'
import './css/app.css'
import './css/foundation-icons.css'
import App from './App'
import reducer from './reducer'

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(thunk))
  
class Main extends React.Component{
  
  unsubscribe = store.subscribe(() => { })
  
  render(){
    return(
      <Provider store={store}>
          <App incomingPrefs={this.props.incomingPrefs}/>
      </Provider>
    )
  }
}