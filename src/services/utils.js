const {origin} = window
const colonLoc = origin.lastIndexOf(':') > 4 ? origin.lastIndexOf(':') : origin.length
const SERVICE_PATH = origin.substr(0,colonLoc) + ":3000"

let counter = 0

const Utils = {

    downloadCSVFor: (list, statusOverride=null)=>{
        const str = list.reduce((prev, video)=>{
            if ( prev === "" ){
                prev = Object.keys(video).reduce((previous,thiskey)=>{
                    return previous + thiskey + ","
                }, "data:text/csv;charset=utf-8,") + "\r"
                
            }
            video.date = Utils.prettyFormatDateForExcel(video.date)
            if ( statusOverride ){
                video.status = statusOverride
            }
            const thisRow = Object.keys(video).reduce((previouskey, thiskey)=>{
                return previouskey + '"' +  unescape(video[thiskey]) + '",'
            },"")
            return prev + thisRow + "\r"
        }, "")
        
        var encodedUri = encodeURI(str);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "kiosk_data.csv");
        link.click();
        
    },

    isKioWare: () => navigator.userAgent.indexOf("KioWare") !== -1,

    now: () => new Date().getTime(),
    
    onDesktop: () => false,
    
    emailValid: (email)=>{
      return true;
      //Customer doesn't want any validation, bad emails just silently fail
      const re = /^(([^<>()[]\\.,;:\s@"]+(\.[^<>()[]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return re.test(email) || email === ""
    },

    getID: () => counter++,

    getDevices: ()=>{
        return fetch(SERVICE_PATH + '/devices',{
            method: 'GET'
          })
    },

    getLabels: ()=>{
        return fetch(SERVICE_PATH + '/labels',{
            method: 'GET'
          })
    },

    getPreferences: ()=>{
        return fetch(SERVICE_PATH + '/preferences',{
            mode: 'cors',
            method: 'GET'
          })
    },

    setPreferences: (pairs)=>{
        return fetch(SERVICE_PATH + '/preferences',{
            method: 'POST',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"pairs": {...pairs} })
          })
    },

    getAllVideos: ()=>{
        return fetch(SERVICE_PATH + '/videos/', {
            mode: 'cors',
            method: 'GET'
          })
    },

    setDevices: (camera, mic)=>{
        return fetch(SERVICE_PATH + '/devices',{
            method: 'POST',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"mic": mic, "cam":camera} )
          })
        

    },
    
    sendPostcard: (bin, to_email, to_name, from_name, 
                    from_service, from_email, from_password, subject)=>{
        var fd = new FormData()
        
        fd.append('to_email', to_email)
        fd.append('to_name', to_name)
        fd.append('from_name', from_name)
        fd.append('bin', bin)
        fd.append('subject', subject)
        fd.append('from_service', from_service)
        fd.append('from_email', from_email)
        fd.append('from_password', from_password)

        return fetch(SERVICE_PATH + '/sendpostcard',{
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*'
              },
            body: fd
          }
        )
    },

    prettyFormatDate: (milliseconds)=>{
        const date = new Date(milliseconds)
        let hour = date.getHours()
        var ampm = 'am'
        if ( hour > 12 ){
            hour -= 12
            ampm = 'pm'
        }
        const minute = date.getMinutes().toString().padStart(2,'0');
        return hour+":"+minute+" "+ampm + " " + date.toDateString(); 
    },

    prettyFormatDateForExcel: (milliseconds)=>{
        //yyyy-MM-dd HH:mm:ss
        const date = new Date(milliseconds)
        return  date.getFullYear() + "-" + 
                (1 + date.getMonth()).toString().padStart(2,'0') + "-" +
                date.getDate().toString().padStart(2,'0') + " " +
                date.getHours().toString().padStart(2,'0') + ":" +
                date.getMinutes().toString().padStart(2,'0') + ":" +
                date.getSeconds().toString().padStart(2,'0');
    },

    archiveVideos: (idlist) =>{
        return fetch(SERVICE_PATH + '/archivevideos',{
            method: 'PUT',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"idlist": idlist} )
          });
    },

    deleteVideos: (idlist) =>{
        return fetch(SERVICE_PATH + '/deletevideos',{
            method: 'DELETE',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"idlist": idlist} )
          });
    },

    getDeletedVideos: ()=>{
        return fetch(SERVICE_PATH + '/deletedvideos/', {
            mode: 'cors',
            method: 'GET'
          });
    },

    undeleteVideos:(idlist) =>{
        return fetch(SERVICE_PATH + '/undeletevideos',{
            method: 'DELETE',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"idlist": idlist} )
          });
    },
    
    permanentlyDelete: (idlist) =>{
        return fetch(SERVICE_PATH + '/deletetrashedvideos',{
            method: 'DELETE',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"idlist": idlist} )
          });
    },
    
    includeInLoop: (idlist, way) =>{
        return fetch(SERVICE_PATH + '/loopvideos',{
            method: 'PUT',
            headers: {
              'Accept': 'application/json, text/plain, */*',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify( {"idlist": idlist, "way": way} )
          });
    },

    saveVideo: (vid, details)=>{
        const  {from_name, from_email, from_phone} = details
        var fd = new FormData()
        fd.append('upl', vid)
        
        var url = new URL(SERVICE_PATH + '/savevid')
        
        const params = {base_name: "visitor_comment_"+(new Date().getTime()), 
                        from_name: from_name, 
                        from_email: from_email, 
                        from_phone: from_phone}

        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

        return fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*'
              },
            body: fd
          }
        )
    },

    mapStateToProps: state => ({
        ...state
    })
}

export default Utils