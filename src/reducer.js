
import {DO_TICK,
    ACTION_SET_CAROUSEL_REF,
    ACTION_VIDEO_ENDED, 
    ACTION_VIDEO_PLAYING, 
    ACTION_SET_SCREEN,
    ACTION_HUMAN_ACTIVITY,
    ACTION_SET_POSTCARD_FRONT,
    ACTION_SET_SNAPSHOT,
    ACTION_POSTCARD_SAVING,
    ACTION_POSTCARD_SAVED,
    ACTION_POSTCARD_ERROR,
    ACTION_STATUS_VIDEO_SAVING,
    ACTION_VIDEO_SAVED,
    ACTION_STALE_THE_DIALOG,
    ACTION_VIDEO_ERROR,
    ACTION_SET_ADMIN_ROW,
    ACTION_ADD_TO_PENDING,
    ACTION_TAKE_OUT_OF_PENDING,
    ACTION_GETTING_VIDEOS,
    ACTION_GOT_VIDEOS,
    ACTION_ERROR_GETTING_VIDEOS,
    ACTION_ADD_SELECTED_VIDEOS,
    ACTION_REM_SELECTED_VIDEOS,
    ACTION_OPEN_VIDEO_PLAYER,
    ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE,
    ACTION_TAKE_OUT_OF_PENDING_UNTRASH,
    ACTION_SET_ADMIN_TAB,
    ACTION_SET_ADMIN_ROW_NEXT,
    ACTION_SET_PREF,
    ACTION_GOT_PREFERENCES,
    ACTION_GOT_DELETED_VIDEOS,
    ACTION_TURN_ON_SUPERUSER,

} from './actions'
import Utils from './services/utils';

const initState = {
  
    superuser: false, 

    videoStopped:true,
    
    dialogConfirmationMessage: "pauses then dismisses self",

    dialogExpiresIn: -1,

    screenKey:'Init', 

    nextTimeout:0,

    preferences: { 
            PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN: 3,
            PREF_COUNTDOWN_BEFORE_VIDEO_STARTS: 3,
            PREF_MAX_VIDEO_DURATION: 15,
            PREF_START_WARNING_AT: 5,
            PREF_INACTIVITY_TIMEOUT: 240,
            from_email: '',
            from_password: '',
            from_service: 'gmail',
            camid: "none",
            micid: "none",
            PREF_POSTCARD_COUNT: 10,
    
            sa_title: "Get Involved!",
            sa_button_video: "Leave your Video Comment",
            sa_button_postcard: "Send a Virtual Postcard",

            pc_email_subject: "sent you a postcard from the John Heinz National Wildlife Refuge",

    
            pc_title:"Send a Virtual Postcard!",
            pc_button_add_snapshot:  "Add a Snapshot!",
            pc_button_on_snapshot: "Add a Snapshot!",
            pc_label_friends_name: "Your Friend's Name:",
            pc_default_friends_name:"Your Friend's Name",

            pc_label_your_name:"Your Name:",
            pc_default_your_name: "Your Name",
            pc_label_friends_email: "Your Friend's Email:",
            pc_label_friends_email_warning: "Your Friend's Valid Email",
            pc_default_friends_email:  "Your Friend's Email",
            pc_button_submit: "Send your Postcard!",
            pc_button_back: "Back",
            pc_button_resnap:"Retake Snapshot",
            
            pc_prompt_smile:"Smile!",
            pc_button_cancel: "Cancel",
            pc_button_retake: "Retake",
            pc_button_accept: "Accept",
            
            pc_dialog_busy: "Sending...",
            pc_dialog_complete: "Success",
        
            v_title:"Record a video comment",
            v_waiver:  "By submitting your recorded video comments, you agree to allow your comments to be viewed by others, including the general visiting public.",
            v_record: "Record",
            v_re_record: "Re-Record",
            v_cancel: "Cancel",
            v_stop: "Stop",
            v_preview: "Preview",
            v_label_name:"Name:",
            v_prompt_name:"Your Name",
            v_label_email:"Email:",
            v_label_email_warning: "Email:",
            v_prompt_email:"Your Email",
            v_submit:"Submit",
           
            v_back:"Back",
            v_countdown: "Starting in ",
            v_countdown_end:"Ending in ",
            v_timesup: "Time's up!",
            v_dialog_busy:"Saving...",
            v_dialog_complete: "Success",
        
    },
    
    postcardFront: './_postcards/front1.png',
    currentError: "",
    currentInfo: "",
    busySavingPostcard: false,
    busySavingVideo: false,
    snap:null,
    selectedRowAdmin: -1,
    pendingList: [],
    videos: [],
    deletedvideos: [],
    selectedVideos:[],
    videoPlayerVideo: null,
    filterTab: 'filter_new',
}


function reducer (state = initState, action) {

    state = {...state, nextTimeout: Utils.now() + (state.preferences.PREF_INACTIVITY_TIMEOUT*1000)}

    switch (action.type) {
        case ACTION_TURN_ON_SUPERUSER:
            return {...state, superuser: true}

        case ACTION_SET_PREF:
            return {...state,
                    preferences: {...state.preferences, [action.data.pref]: action.data.value}
                    }

        case ACTION_GOT_PREFERENCES:
            return {...state, preferences:{...state.preferences, ...action.data}}

        case ACTION_ADD_SELECTED_VIDEOS:
            return {...state, 
                    selectedVideos: [...state.selectedVideos, ...action.data]}

        case ACTION_REM_SELECTED_VIDEOS:
            return {...state, 
                selectedVideos: state.selectedVideos.filter(id=>!action.data.includes(id)) }

        case ACTION_GETTING_VIDEOS:
            return state

        case ACTION_GOT_VIDEOS:
            return {...state, videos: action.data}

        case ACTION_GOT_DELETED_VIDEOS:
            return {...state, deletedvideos: action.data}

        case ACTION_ERROR_GETTING_VIDEOS:
            return {...state, currentError: "Error getting videos " + action.data};

        case ACTION_ADD_TO_PENDING:
            return {...state, 
                    pendingList: [...state.pendingList, ...action.data]}

        case ACTION_TAKE_OUT_OF_PENDING:
            const {idlist, videos} = action.data
            return {...state, 
                    pendingList: state.pendingList.filter(id=>!idlist.includes(id)),
                    videos: videos,
                    selectedRowAdmin: -1,
                    selectedVideos: []}

        case ACTION_TAKE_OUT_OF_PENDING_UNTRASH:
            const {trashids, deletedvideos} = action.data
            return {...state, 
                    pendingList: state.pendingList.filter(id=>!trashids.includes(id)),
                    deletedvideos: deletedvideos,
                    selectedRowAdmin: -1,
                    selectedVideos: []}

        case ACTION_SET_ADMIN_ROW_NEXT:
            const nextidx = state.selectedRowAdmin+1
            const sortedVids = state.filterTab === "filter_loop" ?
                            state.videos.filter(video => video.inloop==='true') :
                            state.filterTab === "filter_trash" ? 
                            state.deletedvideos  :
                            state.videos
            const nextRow2 =   nextidx > sortedVids.length-1 ? -1 : nextidx
            return {...state,
                    selectedRowAdmin: nextRow2,
                    videoPlayerVideo: (nextRow2===-1 ? null : sortedVids[nextRow2]),
                    selectedVideos: []}

        case ACTION_TAKE_OUT_OF_PENDING_AND_ADVANCE:
            const videos2 = action.data.videos;
            const idlist2 = action.data.idlist;
            const justnew = (video)=>(video.status === "new")
            const videosRemaining = videos2.filter(justnew)
            const nextRow = videosRemaining.length===0 ? -1 : state.selectedRowAdmin
            return {...state, 
                    pendingList: state.pendingList.filter(id=>!idlist2.includes(id)),
                    videos: videos2,
                    selectedRowAdmin: nextRow,
                    videoPlayerVideo: (nextRow===-1 ? null : videosRemaining[nextRow]),
                    selectedVideos: []}

        case ACTION_SET_ADMIN_ROW:
            if ( action.data === -1 ){
                return {...state, selectedRowAdmin: -1, selectedVideos: []}
            }else{
                return{...state, selectedRowAdmin: action.data}
            }

        case ACTION_SET_ADMIN_TAB:
            return {...state, 
                    selectedRowAdmin: -1, 
                    selectedVideos: [],
                    filterTab: action.data}

        case ACTION_OPEN_VIDEO_PLAYER:
            return{...state, videoPlayerVideo: action.data}
    
        case ACTION_POSTCARD_SAVING:
            return {...state, busySavingPostcard: true, dialogExpiresIn:1000}

        case ACTION_POSTCARD_SAVED:
            return {...state, busySavingPostcard: true, dialogExpiresIn:2,  dialogConfirmationMessage:state.preferences.pc_dialog_complete}
        
        case ACTION_POSTCARD_ERROR:
            return {...state, busySavingPostcard: false, currentInfo: action.data, screenKey:'Attract'} 
            
        case ACTION_STATUS_VIDEO_SAVING:
            return {...state, busySavingVideo: true, dialogExpiresIn:1000}

        case ACTION_VIDEO_SAVED:
            return {...state, busySavingVideo: true, dialogExpiresIn:4,  dialogConfirmationMessage:state.preferences.v_dialog_complete}
        
        case ACTION_VIDEO_ERROR:
            return {...state, busySavingVideo: false, currentInfo: action.data, screenKey:'Attract'} 
      
        case ACTION_STALE_THE_DIALOG:
            const obj = {...state, 
                busySavingVideo: state.dialogExpiresIn > 0 ? true : false,
                busySavingPostcard: state.dialogExpiresIn > 0 ? true : false,
                            dialogExpiresIn: state.dialogExpiresIn-1 }
            if ( state.dialogExpiresIn < 1 ){
                return {...obj, screenKey:"Attract"}
            }else{
                return obj
            }
                  
        case ACTION_SET_SNAPSHOT:
                return {...state, snap: action.data}

        case ACTION_SET_POSTCARD_FRONT:
            return {...state, postcardFront: action.data}

        case ACTION_HUMAN_ACTIVITY:
            return {...state, nextTimeout: Utils.now() + (state.preferences.PREF_INACTIVITY_TIMEOUT*1000)}
        
        case ACTION_SET_SCREEN:
            return {...state, 
                    screenKey: action.data,
                    filterTab: 'filter_new'}

        case ACTION_VIDEO_ENDED:
            return {...state, videoStopped:true} 

        case ACTION_VIDEO_PLAYING:
                return {...state, videoStopped:false} 

        case ACTION_SET_CAROUSEL_REF:
            return {...state, carouselRef: action.data}

        case DO_TICK:
            return {...state, ticks: action.data}

        default:
            return state
    }
}


export default reducer
