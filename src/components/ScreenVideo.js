import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'
import LiveVideo from './LiveVideo'
import MyButton from './MyButton'
import DialogOverlay from './DialogOverlay'

import {PREF_MAX_VIDEO_DURATION, 
        PREF_START_WARNING_AT,
        PREF_COUNTDOWN_BEFORE_VIDEO_STARTS,
      } from '../actions'

const STATE_STOPPED = "stopped"
const STATE_COUNTDOWN = "counting_down"
const STATE_RECORDING = "recording"
const STATE_PLAYING = "playing"
const MIN_RECORDING_LENGTH = 2

class ScreenVideo extends React.Component{

    state = {
        playstate: STATE_STOPPED, 
        
        currentInterval:-1, 
        timeUntilRecord:-1, //when countdown, this is shown
        elapsedRecordTime: 0, //when record, this is calculated and shown when maxdur-elapsed < 6

        actualvideo: null,
        name: null, 
        email: null,
        did_agree: false,
        agreed_at_least_once: false,
        played_at_least_once: false,
        cameraReady:false,
        email_valid: true,
    }

    onReady = (evt)=>{
      this.setState({cameraReady:true})
    }
    
    clickPreview = (evt) =>{
      this.vid.playrecording()
      //this.refs.vid.playrecording()
      this.setState( {playstate: STATE_PLAYING} )
    }

    clickRecordStop = (evt)=>{
      const {playstate} = this.state

      if ( playstate === STATE_RECORDING ){
        //they clicked stop:
        this.reallyStop()
        return
      }
      if ( playstate === STATE_COUNTDOWN ){
        //they clicked cancel:
        this.reallyCancel()
        return
      }

      if ( playstate === STATE_STOPPED || playstate === STATE_PLAYING  ){
        //start countdown to play:
        this.clearIntervals()
        
        this.vid.showLive()
        //this.refs.vid.showLive()
        
        const interval = setInterval(()=>{
          if ( this.state.timeUntilRecord === 1 ){
              this.reallyRecord()
          }else{
            this.setState(state=>({timeUntilRecord:state.timeUntilRecord-1}))
          }
        }, 1000)

        this.setState({played_at_least_once: true, playstate: STATE_COUNTDOWN, currentInterval: interval, timeUntilRecord: this.pref(PREF_COUNTDOWN_BEFORE_VIDEO_STARTS)})
        return
  
      }
    }
     
    componentWillUnmount(){
       this.clearIntervals()
      }

      clearIntervals(){
        if ( this.state.currentInterval ) clearInterval(this.state.currentInterval)
      }

     reallyStop = ()=>{
      this.clearIntervals()
      
      const videoCounts = ( this.state.elapsedRecordTime >= MIN_RECORDING_LENGTH )
        
        this.vid.stop()
        if ( !videoCounts) this.vid.showLive()
        
        this.setState({actualvideo: videoCounts, playstate: STATE_STOPPED, timeUntilRecord: -1})
      }

     reallyCancel = ()=>{
      this.clearIntervals()
      this.setState({timeUntilRecord: -1, playstate: STATE_STOPPED})
     }

     pref = (key)=>this.props.preferences[key] 
     

     reallyRecord = ()=>{
      this.clearIntervals()
      this.vid.record()
      //this.refs.vid.record()
      const interval = setInterval(()=>{
        if ( this.state.elapsedRecordTime > this.pref(PREF_MAX_VIDEO_DURATION) ){
          this.reallyStop()
        }else{
          this.setState(state=> ({elapsedRecordTime: state.elapsedRecordTime + 1}))
        }
      },1000)
      this.setState({currentInterval: interval, playstate:STATE_RECORDING, timeUntilRecord:-1, elapsedRecordTime: 0})

     }

    validateText = (evt)=>{
      const name = document.getElementById('txt_name').value
      const email = document.getElementById('txt_email').value
        
      const did_agree = document.getElementById('waiver_checkbox').checked
      
        const email_valid = Utils.emailValid(email)
        if ( did_agree ){
          this.setState({name, email, email_valid, did_agree, agreed_at_least_once:true})
        }else{
          this.setState({name, email, email_valid, did_agree})
        }
    }
    
    validated = ()=> {

      return  this.state.did_agree &&
              this.state.name &&  
              this.state.email && 
              this.state.actualvideo && 
              (this.state.playstate===STATE_STOPPED || this.state.playstate===STATE_PLAYING )
    }

    onNav = (idVal) =>{
      this.props.dispatch(actions.setScreen(idVal))
    }

    onSubmitVideo = (evt)=>{
      this.props.dispatch(actions.saveVideo(this.vid.getBin(), 
                                            {from_name: escape(this.state.name), 
                                            from_email: escape(this.state.email) }))
    }
    
    label = (key)=> this.props.preferences[key] || ""
    
  render(){

    const blurit = this.state.agreed_at_least_once ? " " : " blurit"
    const transback = this.state.agreed_at_least_once ? " " : "transback" 

    const remainingTime = Math.max(0, this.pref(PREF_MAX_VIDEO_DURATION)-this.state.elapsedRecordTime);
    const countupDisplay = this.state.playstate===STATE_RECORDING ?
                          (this.state.elapsedRecordTime > 0 && remainingTime<(this.pref(PREF_START_WARNING_AT)+1) ? (remainingTime===0?this.label('v_timesup'): this.label('v_countdown_end')+remainingTime): "") : ""
                          
    const countdownDisplay = this.state.playstate===STATE_COUNTDOWN ?
                          this.label('v_countdown')+this.state.timeUntilRecord  : ""
     
    const top = 0, left=0
    


    return (
      <div className='videoscreen'>
     
        
        <div className={transback + " grid-y fullscreen " + ((this.props.busySavingVideo) ? " blurit " : "")}>

          {!Utils.isKioWare() ? <div className="proof-mode"/> : null}
        
          <div className="small-2 cell">
            <h1 className="specialfont-small">{this.label('v_title')}</h1>
          </div>

          <div className="small-8 cell grid-x grid-padding-x">
                
                <div className="small-8 cell flex-container" style={{textAlign: 'left'}}>
                  <div className="flex-child-auto relative_container">
                    <LiveVideo ref="vid" 
                        style={{width: '92%', position: 'absolute', left: '40px'}}
                          onRef={ref => (this.vid = ref)}
                          playstate={this.state.playstate} 
                          onReady={this.onReady}
                          outlineColor={this.state.playstate === STATE_RECORDING ? 
                                        (this.state.elapsedRecordTime%2===0?'red':'pink') : 
                                        this.state.playstate === STATE_COUNTDOWN ? 'orange' : 'gray'}/>
                  

                    
                    <div className="countdown-container">
                    
                    { !(this.state.agreed_at_least_once) ?
                        <div className="big-arrow fi-arrow-down arrow-prompt-waiver" /> : 
                          
                        !(this.state.played_at_least_once) ?
                            <div className="big-arrow fi-arrow-right arrow-prompt-start" /> : 
                            null 
                    }
                 
                      <div className="avoid-clicks countdown" style={{top,left}} >{countdownDisplay}</div>
                      <div className="avoid-clicks countup" style={{top,left}}  >{countupDisplay}</div>
                    </div>  
                  </div>
                </div>

                <div className="small-4 cell " >
                    <div className="grid-y grid-padding-y">
                      
                          <MyButton 
                                  className={"cell "+blurit}
                                  fill={this.state.playstate===STATE_RECORDING ? 'red' : null}
                                  label={this.state.playstate === STATE_RECORDING? this.label('v_stop') : (this.state.timeUntilRecord===-1 ? (this.state.actualvideo ? this.label('v_re_record') : this.label('v_record') ) : this.label('v_cancel'))}

                                  disabled={this.props.busySavingVideo || !this.state.agreed_at_least_once || !this.state.cameraReady}
                                  
                                  click={this.clickRecordStop}/>

                                  
                          <MyButton 
                                  className={"cell "+blurit}
                                  disabled={this.props.busySavingVideo ||  !this.state.playstate === STATE_STOPPED  || !this.state.actualvideo || this.state.playstate === STATE_COUNTDOWN}
                                  label={this.label('v_preview')}
                                  click={this.clickPreview}/>



                          
                      
                      <div className={"small-3 cell fluid "+blurit}>  
                            <label className="field-label">{this.label('v_label_name')}
                            </label> <input  
                                    maxLength="92"
                                    disabled={!this.state.agreed_at_least_once}
                                    spellCheck="false" placeholder={this.label('v_prompt_name')} onChange={this.validateText} id="txt_name" name="txt_name" type="text"/>
                           

                          
                            <label className={this.state.email ? (this.state.email_valid ? 'field-label' : 'wavy field-label') : 'field-label'} >{this.state.email_valid ? this.label('v_label_email'):this.label('v_label_email_warning')}
                            </label><input  
                                    maxLength="56"
                                    disabled={!this.state.agreed_at_least_once}
                                    spellCheck="false" placeholder={this.label('v_prompt_email')} onChange={this.validateText} id="txt_email" name="txt_email" type="text"/>
                            
                       
                       
                      </div>

                        <MyButton 
                          className={"cell "+blurit}
                          disabled={this.props.busySavingVideo || !this.validated()}
                          label={this.label('v_submit')}
                          click={this.onSubmitVideo}/>
                    

                        <div className="cell grid-x">
                          <div className="cell small-6"/>
                          
                          <div className="auto small-6  container-bottom-right">
                            <MyButton 
                                        disabled={this.props.busySavingVideo}
                                        className="cell"
                                        label={this.label('v_back')}
                                        id="Attract"
                                        click={this.onNav}/>
                            </div>
                                        
                        </div>
                    
                     </div>




                    </div>
              </div>

              <div className="cell small-2" >
                        <div className="grid-x grid-padding-x grid-padding-y flex-container">
   
                            <div className='cell small-8 grid-x relative_container' style={{textAlign: 'left', height: '90%'}}>
                              <div className='cell small-2'> 
                                  <input onClick={this.validateText} id="waiver_checkbox" type="checkbox" className="checkbox-big"></input>
                              </div>
                              <h2  className="cell small-8 checkboxtext avoid-clicks" >{this.label('v_waiver')}</h2>
                            </div>
                        </div>
              </div>
             
             

          </div>
        
        
        { this.props.busySavingVideo ? 
          
          <DialogOverlay initialMessage={this.label('v_dialog_busy')} {...this.props} />
          
          :

          null}

          </div>
    )
  }
  
}

export default connect(Utils.mapStateToProps)(ScreenVideo)
