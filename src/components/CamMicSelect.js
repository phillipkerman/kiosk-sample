import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'

class CamMicSelect extends React.Component{

  state = {cams: [], mics:[], id:null}
  mounted = false;

  getID = () =>{
    if ( this.props.type === "camid"){
        const camSelect = document.querySelector('.camSelect')
        return camSelect.options[camSelect.selectedIndex].value
       
    }else{
        const micSelect = document.querySelector('.micSelect')
        return micSelect.options[micSelect.selectedIndex].value        
    }
  }

  onChange = (evt) =>{
    const id = this.getID()
    
    this.setState({id})
    this.props.onChange({pref: this.props.type, value:id})

  }

  componentWillUnmount(){
    this.mounted = false;
  }

  componentDidMount(){
        this.mounted = true;
        navigator.mediaDevices.enumerateDevices()
        .then(arr => {
                if ( !this.mounted ) return
                const notdefault = (item)=>(item.deviceId!=="communications" && item.deviceId!=="default")
                const labelAndId = (item)=>({label: item.label ? item.label : "-no-label " + item.deviceId, id: item.deviceId})
                
                const mics = arr.filter(item=>item.kind==="audioinput").filter(notdefault).map(labelAndId)
                const cams = arr.filter(item=>item.kind==="videoinput").filter(notdefault).map(labelAndId)
                
                this.setState({
                    mics, cams
                })

                if ( this.props.onInit ){
                    const id = this.getID()
                    this.setState({id})
                    this.props.onInit({pref: this.props.type, value:id})
                }

            }
        )
    }
  
 

  render(  ){
    const {camid, micid} = this.props.inits
    
    
    if ( !this.props.allowed){
        return ( <div className="grid-x container" style={{width:'100%', height: '50px'}}>
                    <span className="label-pref">{this.props.type} on kiosk only</span>
                </div>)
    }

    return (  
        <div className="grid-x container" style={{width:'100%'}}>
        <span className="label-pref">{this.props.type}</span>
            { ( this.props.type === "camid") ?
            
                <select onChange={this.onChange} className="cell camSelect" id='camSelect'>
                    {(this.state.cams.map((camera, idx)=>{
                        const camSelected = camid===camera.id
                        return  <option defaultValue={camSelected} key={'c'+idx} id={'c'+idx} value={camera.id}>{camera.label}</option>
                    }))}
                </select>

        :

        
                <select onChange={this.onChange} className="cell  micSelect" id='micSelect'>
                    {(this.state.mics.map((mic, idx)=>{
                    const micSelected = micid===mic.id
                        return <option defaultValue={micSelected} key={'m'+idx} id={'m'+idx} value={mic.id}>{mic.label}</option>
                    }))}
                </select>
            }

        </div>
    ) 
  }
}

export default connect(Utils.mapStateToProps)(CamMicSelect)

