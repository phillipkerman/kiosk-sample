import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'
import SnapshotOverlay from './SnapshotOverlay'
import actions from '../actions';
import html2canvas from 'html2canvas'
import MyButton  from './MyButton'
import DialogOverlay  from './DialogOverlay'
import PostcardScroller from './PostcardScroller'

class ScreenPostcardDetails extends React.Component{
    
    state = {email_to:"", 
             name_to:"", 
             name_from:'', 
             scrollIndex: 0,
             selectedIndex:0,
             moving: 0,
             email_valid: true,  
             overlayOpen:false}
 
    componentWillUnmount(){
        this.props.dispatch(actions.saveSnapshot(null))
        const defaultFront = './_postcards/front1.png'
        this.props.dispatch(actions.setPostcardFront(defaultFront))
    }

    toggleSnapshopOverlay = (evt) =>{
        if ( this.props.busySavingPostcard ) return
        this.setState({overlayOpen: !this.state.overlayOpen})
    }

    validateText = (evt) =>{
        const name_from = document.getElementById('txt_from').value
        const name_to = document.getElementById('txt_to').value
        const email_to = document.getElementById('txt_email').value
        const email_valid = Utils.emailValid(email_to)
        this.setState({name_from, name_to, email_to, email_valid})
    }
    
    validated = () =>{
        return this.state.name_from && this.state.name_to && this.state.email_to
    }

    onClickThumb = (evt)=>{
        this.props.dispatch(actions.setPostcardFront(evt.target.src))
    }
  
    doCancel = (evt) =>{
        this.setState({overlayOpen: false})
    }

    onSendPostcard = ()=>{

        //show "waiting.."
        this.props.dispatch(actions.startPostcard())

        //figure out crop marks:
        const oneCard = document.querySelector('#cardfront').getBoundingClientRect()
        const container = document.querySelector('#bothcards').getBoundingClientRect()
        //const margin = Math.floor(  (container.width - oneCard.width) / 2 )
        
        if ( !this.props.snap ){
            document.querySelector('#poscardsnap').src = './_postcards/default_snap.png'
            document.querySelector('#snapprompt').classList.add('invisi')
        }
        html2canvas( document.querySelector("#bothcards"), 
                     {x:oneCard.x, y:oneCard.y, width:oneCard.width, height:container.height} )
            .then(canvas => {
           // document.body.appendChild(canvas)
          
            const {from_service, from_email, from_password, pc_email_subject} = this.props.preferences
            const subject = this.state.name_from + " " +pc_email_subject
             
            this.props.dispatch(actions.sendPostcard(canvas.toDataURL(), 
                                                    this.state.email_to,
                                                    this.state.name_to,
                                                    this.state.name_from,
                                                    from_service, 
                                                    from_email, 
                                                    from_password,
                                                    subject))
        });

    }      

    onNav = (screen) =>{
        this.props.dispatch(actions.setScreen(screen))
    }
    
    label = (key)=>this.props.preferences[key] || ""

    textToHtml = (html)=>{
        let arr = html.split(/<br\s*\/?>/i);
        return arr.reduce((el, a) => el.concat(a, <br />), []);
    }
    
    label = (key)=>this.props.preferences[key] || ""
                   
  render(){
  
    return (
    <div className="fullscreen">
        
        {!Utils.isKioWare() ? <div className="proof-mode"/> : null}
      
        <div className={(this.state.overlayOpen || this.props.busySavingPostcard ? 'blurit ' : '' )+'grid-y fullheight2'} >
        
            <div className="cell small-2">   
                <h1 className="specialfont">{this.label('pc_title')}</h1>
            </div>
        
        <div className="cell small-10">
            <div className="grid-x grid-padding-x">
            
            <div className="cell small-3"  style={{marginTop: '-20px'}}> 
                <PostcardScroller   />
            </div>

            <div className='cell small-5' >
                <div className="grid-y grid-padding-y grid-container" id='bothcards'>
                            <div className="auto small-6">
                                <div className="relative_container" >
                                    <img className="cardimage" id="cardfront" alt="" src={this.props.postcardFront}/>
                                </div>
                            </div>
                            <div style={{height: '20px'}}/>
                            <div className="auto small-6" >
                                <div className="relative_container" >
                                    <img className="cardimage" alt="" src={'./_postcards/postcard_back.png'}/>
                                    <div  onClick={this.toggleSnapshopOverlay}>
                                        <img alt="" id="poscardsnap" className={"postcard-snap "}  src={this.props.snap}/>
                                        {this.props.snap ? null : <div id="snapprompt" className="avoid-clicks postcard-prompt">{this.label('pc_button_on_snapshot')}</div>}
                                    </div>
                                    <div className="postcard-caption avoid-clicks">{false?'hold spot for dyn caption':null}</div>
                                    
                                    <div id="id_to" className="postcard-address-to avoid-clicks">{this.state.name_to}</div>
                                  
                                    <div  id="id_from" className="postcard-address-from avoid-clicks">{this.state.name_from}</div>
                                </div>
                            </div>
                </div>
            </div>   


            <div className='cell small-4'>
                <div className="grid-y grid-padding-y">


                    <div className="cell small-8">
                        <MyButton  disabled={this.props.busySavingPostcard}
                                    style={{ width: '100%'}}
                                    click={this.toggleSnapshopOverlay} 
                                    label={this.props.snap ? this.label('pc_button_resnap') : this.label('pc_button_add_snapshot')}
                                    id="Snapshot" />

                    
                        <label className="field-label">{this.label('pc_label_friends_name')}</label>
                        <input  maxLength="92"
                                spellCheck="false" placeholder={this.label('pc_default_friends_name')} onChange={this.validateText} id="txt_to" name="txt_to" type="text"/>
                        

                        <label className="field-label">{this.label('pc_label_your_name')}</label>
                        <input  maxLength="92"
                                spellCheck="false" placeholder={this.label('pc_default_your_name')} onChange={this.validateText} id="txt_from" name="txt_from" type="text"/>
                        
                    
                    
                        <label className={this.state.email_to ? (this.state.email_valid ? 'field-label' : 'field-label wavy') : 'field-label'} >{this.state.email_valid ? this.label('pc_label_friends_email') :  this.label('pc_label_friends_email_warning')}  </label>
                        <input  maxLength="56"
                                spellCheck="false" placeholder={this.label('pc_default_friends_email')}  onChange={this.validateText} id="txt_email" name="txt_email" type="text"/>
                

                        <MyButton  style={{ width: '100%'}}
                                    click={this.onSendPostcard} 
                                    label={this.label('pc_button_submit')}
                                    id="Snapshot"
                                    disabled={this.props.busySavingPostcard || !this.validated()}/>
                    
                    </div>

                    <div className="cell small-4 grid-x" >
                    
                          <div className="cell small-6"/>
                          
                          <div className="auto small-6  container-bottom-right">
                            
                                <MyButton   className="cell"
                                            disabled={this.props.busySavingPostcard}
                                            click={this.onNav} 
                                            label={this.label('pc_button_back')}
                                            
                                            id="Attract"/>
                           
                          </div>

                        
                    </div>
                    
                </div>

            </div>          
            </div>
            
     

            </div>

            

        </div>
        
        { this.props.busySavingPostcard ? 
             <DialogOverlay initialMessage={this.label('pc_dialog_busy')} {...this.props} />
             :
             null}


        { this.state.overlayOpen ? 
            <SnapshotOverlay 
                doCancel={this.doCancel} doAccept={this.doAccept}/>
            :
            null
        }

        </div>
      
    )
  }
  
}

export default connect(Utils.mapStateToProps)(ScreenPostcardDetails)