import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'
import LiveVideo from './LiveVideo'
import actions from '../actions'
import MyButton from './MyButton'
import {PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN} from '../actions'



class SnapshotOverlay extends React.Component{
    
    snapdata = null
    intervalid = -1
    state = {snap:null, timeUntilSnap:""}

    startCountdown=(evt)=>{
        this.clearSnap()
        this.setState({snap: false, timeUntilSnap: this.props.preferences[PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN]})
        this.livevideo.show()

        this.intervalid = setInterval(()=>{
            if ( this.state.timeUntilSnap === 0 ){
                clearInterval(this.intervalid)
                this.doSnap()
                this.setState({timeUntilSnap:-1, snap:true})
            }else{
               this.setState({timeUntilSnap:this.state.timeUntilSnap-1})
            }
        }, 1000)

    } 

    doCancel = (evt) =>{
        this.props.doCancel()
    }

    doAccept = (evt) =>{
        //these won't work
        //this.props.saveGlobalHandler({snap: this.snapdata})
        this.props.dispatch(actions.saveSnapshot(this.snapdata))
        this.props.doCancel()
    }

    clearSnap(){
        
        const canvas = document.getElementById('stillcanvas')
        const context = canvas.getContext('2d')
        context.fillStyle = "aquamarine";
        context.fillRect(0, 0, canvas.width, canvas.height);
    
    }

    doSnap(){
        const canvas = document.getElementById('stillcanvas')
        var context = canvas.getContext('2d');
        const el = this.livevideo.getVideoElement()
        context.drawImage(el, 0, 0, canvas.width, canvas.height);
        this.snapdata = canvas.toDataURL('image/png');
        this.livevideo.hide()
    }

    componentDidMount=()=>{
        console.log("did mount!")
       
    }
    onReady = ()=>{
        console.log("ready!") 
        this.startCountdown()
    }

    componentWillUnmount=()=>{
        if ( this.intervalid ) clearInterval(this.intervalid)
    }

    label = (key)=>this.props.preferences[key] || ""

    render(){
        const info = {width: 640, height:480}
    
        return (
            <div>
                <div className="overlayback">
                <div className="grid-y grid-padding-y fullheight2">
                
                    <div className="cell small-8">
                        <div className='grid-x  grid-padding-x'>
                            <div className='cell small-6' >

                                <LiveVideo width="640" 
                                            height="480" 
                                            info={info} 
                                            onReady={this.onReady}
                                            onRef={ref => (this.livevideo = ref)} 
                                            id="videoid"/>  
                                <div className="video-container">
                                    <canvas   className="regular-video"
                                        hidden={!this.state.snap} id="stillcanvas" 
                                    width="640" height="480"></canvas>
                                </div>
                                <div className="avoid-clicks countdown">
                                                {this.state.timeUntilSnap===0 ? this.label('pc_prompt_smile') : this.state.timeUntilSnap > -1 ? this.state.timeUntilSnap : ""}
                                </div>   
                            </div> 
                            
                        </div>
                    </div>

                    <div className="cell small-4 flex-container flex-dir-column">
                        
                                <div className="cell flex-child-auto">
                                    <MyButton   
                                        click={this.doCancel} 
                                        label={this.label('pc_button_cancel')}
                                        id="PostcardDetails"/>
                                </div>
                                <div className="cell flex-child-auto">
                                    <MyButton   
                                        disabled={!this.state.snap} 
                                        click={this.startCountdown} 
                                        label={this.label('pc_button_retake')}/>
                                </div>
                                <div className="cell flex-child-auto">
                                    <MyButton   
                                        disabled={!this.state.snap}
                                        click={this.doAccept} 
                                        label={this.label('pc_button_accept')}/>
                                </div>
                        
                    </div>
                        
                                
                    </div>
                </div> 
            </div>  
        )
  }
}

export default connect(Utils.mapStateToProps)(SnapshotOverlay)