import React from 'react'
import actions from '../actions'
import { connect } from 'react-redux'
import Utils from '../services/utils'

class CarouselItem extends React.Component{
    
    vid = null

    state = {videoEnded: false}

    dispatchEnd = ()=>{
        this.setState({videoEnded:true})
        this.props.dispatch(actions.carouselVideoEnded())
    }

    isActive = ()=>(false && this.props.playingIndex === this.props.idx && !this.props.pauseVideo)
    
    render(){
        const imActive = this.isActive()
        
        const {vid, src} = this.props

        const className = imActive  ? 'video-fit highlight-carousel-current' : 'video-fit'
      
        return (
            <div className='carousel-item-container' onClick={this.onClick}>
            {
              imActive ?
                    <video id={vid} className={className} controls={false} poster={src}
                    onEnded={this.dispatchEnd} src={vid} autoPlay={true}/>
              

                 :
                
                <img  className='video-fit'  alt="" src={this.props.src}/>
            }
            </div>
        )
  }
}

export default connect(Utils.mapStateToProps)(CarouselItem)
