import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'
import CamMicSelect from './CamMicSelect'
import MyButton from './MyButton'
import InputText  from './InputText'


class ScreenInit extends React.Component{

  state = {cams: [],
          mics:[], 
          camid:null, 
          micid:null, 
          from_service:null, 
          from_email:null, 
          from_password:null}

  onNav = (evt) =>{
    this.props.dispatch(actions.refreshVideoList(evt.target.id))
  }

  easyGo = ()=>{
    this.onNav({target: {id: "Attract"}})
  }

  handleChange = (info) =>{
    const obj = {[info.pref]: info.value}
    this.setState(state => ({...state, ...obj}))
  }

  handleInit = (info)=>{
    const obj = {[info.pref]: info.value}
    this.setState(state => ({...state, ...obj}))
  }

  componentWillMount(){
    
    if ( !Utils.isKioWare() ){
      this.onNav({target: {id: "Admin"}})
      return
    }
    const {camid,micid,from_email,from_password,from_service} = this.props.preferences
   
    if ( !camid ||
         camid==="none" ||
         !micid ||
         micid==="none" ||
         !from_email  ||
         !from_password  ||
         !from_service   ){
          
          this.setState({camid,micid,from_email,from_password,from_service})    

    }else{
      this.easyGo()
    }
  }

  isValid = ()=>{
    const {camid,micid,from_email,from_password,from_service} = this.state 
   
    return !(!camid ||
      camid==="none" ||
      !micid ||
      micid==="none" ||
      !from_email  ||
      !from_password  ||
      !from_service )
  }
       
  handleSubmit = (evt)=>{
    const {camid, micid, from_email, from_password, from_service} = this.state
    const obj = {camid, micid, from_email, from_password, from_service}
    Utils.setPreferences(obj)
    .then((res)=>{
        if ( res.ok ){
            res.json().then((json)=>{
                //console.log(json)
                //
                this.props.dispatch(actions.gotPreferences(obj))
    
                this.easyGo();
            })
        }
    })
  }

  render(  ){
    const {camid,micid,from_email, from_password } = this.props.preferences
    if ( from_email === null && from_password === null ){
      console.log("null")
    }
    return (
      <div className='fullscreen grid-x' >
        <div className="cell grid-container">
       
            <div>
                <h1>select required preferences {this.props.currentError}</h1>
                <div className="auto grid-x">
                  <MyButton 
                            className="auto"
                              size='small'
                              label="save"
                              id='put-in-archive'
                              disabled={!this.isValid()}
                              click={this.handleSubmit}/>
                  <CamMicSelect allowed={true} className="auto" type="camid" onInit={this.handleInit} onChange={this.handleChange} inits={{camid}} {...this.props}/>
                  <CamMicSelect allowed={true} className="auto" type="micid" onInit={this.handleInit} onChange={this.handleChange} inits={{micid}} {...this.props}/>
                </div>

                <form className="auto grid-y">
                  <div className="auto "> 
                      <InputText 
                              onChange={this.handleChange}
                              label="email username"
                              pref={'from_email'} > 
                      </InputText>
                  </div>
                  <div className="auto ">
                      <InputText 
                              onChange={this.handleChange}
                              className=" auto"
                              label="email password"
                              password={true}
                              pref={'from_password'} > 
                      </InputText>
                  </div>
                  <div className="auto ">
                      <InputText 
                              onChange={this.handleChange}
                              className=" auto"
                              label="transport service (gmail)"
                              pref={'from_service'} > 
                      </InputText>
                  </div>
                </form>
            </div>
        </div>
      </div>
    )
  }
  
}

export default connect(Utils.mapStateToProps)(ScreenInit)

