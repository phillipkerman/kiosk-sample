import React from 'react'
import Coverflow from 'react-coverflow'
import CarouselItem from './CarouselItem'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'
import MyButton from './MyButton'
class ScreenAttract extends React.Component{

  state = {
    active: 0,
    playingIndex: -1,
    interval:-1,
    lateStartTimeout: -1,
    manuallyAdvancing: false,
    pauseVideo: false,
  }

  clickLeftArrow = (evt)=>{
    evt.preventDefault()
    this.goNext(null, -1);   
  }

  clickRightArrow = (evt)=>{
    evt.preventDefault()
    this.goNext(null, 1);   
  }

  componentDidMount(){
    const el = document.querySelector('.coverflow-container')
    const child = el.children[0]
    const loopvideos = this.props.videos.filter(video=>video.inloop==='true')
    const nm = loopvideos.length % 2 === 0 ? 'even-attract-loop' : ''
    child.className = nm
    
  }

  componentWillMount(){
    this.props.dispatch(actions.pollPreferences())
    this.props.dispatch(actions.refreshVideoList()) 
    this.props.dispatch(actions.carouselVideoEnded())

    const interval = window.setInterval(()=>{
      if ( this.props.videoStopped && !this.state.manuallyAdvancing ) {
        this.goNext()
      }
    },2000)
    
    this.setState({interval,manuallyAdvancing: false})
  }


  goNext = (evt, dir=1)=>{
    const manuallyAdvancing = (evt===null)
    const pauseVideo = (evt===null)
    this.props.dispatch(actions.carouselVideoStarted())

    const len = this.props.videos.filter(video=>video.inloop==='true').length;
    const active = (this.state.active + dir) >= len ? 0 : (this.state.active + dir) < 0 ? len-1 : this.state.active + dir;

    const lateStartTimeout = window.setTimeout(()=>{
        this.setState({playingIndex:active, manuallyAdvancing: false, pauseVideo:false})
      
    }, 1000)

    this.setState({lateStartTimeout, active, pauseVideo, manuallyAdvancing})
  }
  
  onClick = (evt)=>{
    evt.preventDefault()
    evt.stopPropagation()
    
    if (evt.ctrlKey){
      if (evt.shiftKey ) this.props.dispatch(actions.goSuperUser())
      window.clearTimeout(this.state.lateStartTimeout)
      this.onNav('Admin')
    }
  }

  componentWillUnmount(){
    window.clearInterval(this.state.interval)
    window.clearTimeout(this.state.lateStartTimeout)
  }

  onNav = (screen) =>{
    this.props.dispatch(actions.setScreen(screen))
  }

  onOver = (evt) =>{
    evt.target.classList.add("rolloverbutton")
  }

  onOut = (evt) =>{
    evt.target.classList.remove("rolloverbutton")
  }

  label = (key)=>this.props.preferences[key] || ""
  
  render(  ){
    const loopvideos = this.props.videos.filter(video=>video.inloop==='true')
    const {active} = this.state

    return (
      <div className="grid-y fullscreen" onClick={this.onClick} >

        {!Utils.isKioWare() ? <div className="proof-mode"/> : null}
          
          <div className='auto container' >
            <h2 className="specialfont" style={{marginTop: '35px'}}>{this.label('sa_title')}</h2>
          </div>
       
          <div className="auto coverflow-container"  style={{height:'100%'}}  >
    
                <Coverflow
                  //ref={ref => this.coverflow = ref}
                  className='coverflow avoid-clicks'
                  
                  id="xx"
                  width={'100%'}
                  height={'1080'}
                  displayQuantityOfSide={1}
                  navigation={false}
                  enableHeading={false}
                  active={active}
                  clickable={false}
                  enableScroll={false}
                  currentFigureScale={1.5}
                  otherFigureScale={0.8}
                >

                {loopvideos.map( (item, idx) =>(
                    <CarouselItem  playingIndex={this.state.playingIndex}
                                    pauseVideo={this.state.pauseVideo}
                                  idx={idx} 
                                  key={idx}
                                  vid={item.video} 
                                  src={item.thumb}/>
          
                ))}
           
     

              </Coverflow> 
          </div>
      
              
          <div className="auto  shrink container grid-y" 
          style={{display: 'flex', height: '20%', marginTop: '-70px'}} >
                <div className="auto grid-x attract-loop-arrows">
                  <div className="big-arrow fi-arrow-left" onClick={this.clickLeftArrow}/>
                    <div className="attract-loop-arrow-spacer" />
                  <div className="big-arrow fi-arrow-right" onClick={this.clickRightArrow}/>
                </div>


                <div className="auto grid-x attract-loop-buttons" >
                    <MyButton  className="flex-child-auto"   
                                  style={{width:'580px'}}
                                  click={this.onNav} 
                                  label={this.label('sa_button_video')}
                                  id="Video" />

                          <div className="attract-loop-button-spacer" />
                          
                    <MyButton  className="flex-child-auto"   
                                style={{width:'580px'}}
                                click={this.onNav} 
                                label={this.label('sa_button_postcard')}
                                id="PostcardDetails" /> 
                 
                </div> 

        </div>

        
      </div>
   
    )
  }
  
}

export default connect(Utils.mapStateToProps)(ScreenAttract)