import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'
import MyButton from './MyButton'

class  VideoPlayerOverlay extends React.Component{
 
    state = {wantMoreInfo:false}

    onClick = (evt)=>{
        this.props.dispatch(actions.clickVideoInRow(null))
    }

    onClickForMoreInfo = (evt)=>{
        evt.stopPropagation()
        this.setState({wantMoreInfo:true})
    }

    goNext = (tracker, evt)=>{
        evt.stopPropagation()
        this.props.dispatch(actions.nextAdminRow())
    }

    tagIt = (tracker, evt)=>{
        evt.stopPropagation()
        switch(tracker){
            case "put-in-loop":
                this.props.dispatch(actions.putVideosInLoop([this.props.video._id], 
                                                             this.props.video.inloop!=='true',
                                                             true))
                break;
            case "put-in-archive":
                this.props.dispatch(actions.archiveVideos([this.props.video._id],
                                                        true))
                break;
            default:
                break;
        }

    }
  
    getPromptButtons=()=>{
        const {video} = this.props
        const disabled = this.props.pendingList.includes(video._id) 
       // const w = "200px"
        //really if it just has any pending
       
        const buttonArchive = <MyButton  style={{width: '200px'}}
                                disabled={disabled}
                                size='small'
                                className="cell"
                                label={"Archive "}
                                id='put-in-archive'
                                click={this.tagIt}/>
       
        const buttonLoopIn = <MyButton style={{width: '200px'}}
                                disabled={disabled}
                                className="cell"
                                size='small'
                                label={"Move into loop"}
                                id='put-in-loop'
                                click={this.tagIt}/>
                                
        const buttonNext = <MyButton  style={{width: '200px'}}
                            size='small'
                            className="cell"
                            label={"Next "}
                            id='doNext'
                            click={this.goNext}/>
                            
        if ( video.status === "new" ){   
            return (<div className="grid-container grid-y admin-button-container" >
                        {buttonArchive}
                        {buttonLoopIn}
                    </div>)
        }else{
            return (<div className="grid-container grid-y admin-button-container" >
                        {buttonNext}
                    </div>)

        }
    }


    render(){
        const {video} = this.props
       
        return (
            <div className="admin-video-overlay grid-container"
                onClick={this.onClick} >

                <div className="admin-video-container grid-y">
                       
                        <div className='small-9 grid-x grid-padding-y' >

                            <div className="small-3">
                            </div>
                            <div className="small-6">
                            <video className="admin-video"   height="480"
                                controls 
                                src={video.video}  
                                autoPlay
                                id="vid"></video>
                            </div>
                            <div className="auto small-3">
                                {this.getPromptButtons()}
                            </div>
                        </div>

                        <div className="small-3 grid-container video-details-container"
                            onClick={this.onClickForMoreInfo}>
                            <div className="video-details"  
                            onClick={(evt)=>evt.stopPropagation()}>
                                <div >name: {unescape(video.guest_name)}</div>
                                <div >email: {unescape(video.email)}</div>
                                <div >{Utils.prettyFormatDate(video.date)}</div>
                                {this.state.wantMoreInfo ? <div>{video._id}</div> : null }

                                <div className={(Utils.isKioWare() ? "hidden-display" : "")}><a href={video.video} download>download original</a></div>
                        
                            </div>
                        </div>    
                </div>

            </div>

        )
    }
}

export default connect(Utils.mapStateToProps)(VideoPlayerOverlay)