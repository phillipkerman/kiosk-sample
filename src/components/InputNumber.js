import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'

class InputNumber extends React.Component{

    state = {value:null}

    componentWillMount(){
        this.setState({value: this.props.preferences[this.props.pref]})
    }

    onChange = (evt)=>{
        const {pref} = this.props
        const value = Number(evt.target.value)
        this.setState({value})
        this.props.onChange({pref, value})
    }

    render(){
       const {value} = this.state
        return (
            <div className="auto cell grid-x" >
                <div className="auto small-6 cell" >
                    <span className="label-pref avoid-clicks">{this.props.label}</span>
                    <input onChange={this.onChange} id="number" type="number" value={value}></input>
                </div>
            </div>
        )
    }
}


export default connect(Utils.mapStateToProps)(InputNumber)