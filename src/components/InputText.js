import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'

class InputText extends React.Component{

    state = {value:null}

    componentWillMount(){
        this.setState({value: this.props.preferences[this.props.pref]})
    }

    onChange = (evt)=>{
        const {pref} = this.props
        const value = (evt.target.value)
        this.setState({value})
        this.props.onChange({pref, value})
    }
          
    render(){
       const {value} = this.state
        return (
            <div style={{width:'100%'}}  className="auto">
                <span className="label-pref avoid-clicks">{this.props.label}</span>
                <input autoComplete="none" className={this.props.tall?'field-tall':'field-normal'} onChange={this.onChange} id={this.props.pref} type={this.props.password ? "password" : "text"} value={value}></input>
            </div> 
        )
    }
}

export default connect(Utils.mapStateToProps)(InputText)