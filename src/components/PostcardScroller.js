import React from 'react'
import actions from '../actions'
import { connect } from 'react-redux'
import Utils from '../services/utils'

const ANIM_DUR = 450 //tied to CSS scrollit and freezeit
const TOUCH = {}
class PostcardScroller extends React.Component{
        
    state = {moving: 0, 
            currentIndex:0,
            doneMoving: -1
             }
    interval = -1
    
    getListBasedOn = (list, index)=>{
        const getOffset = (idx)=>{
            return list[this.safe(list, idx)]
        }
        return [ getOffset(index-2), getOffset(index-1), list[index], getOffset(index+1), getOffset(index+2)]
    }
    
    safe = (list, idx)=>{ 
        if ( idx > (list.length-1) ){
            idx = idx-list.length
        }
        if ( idx < 0 ){
            idx = list.length+idx
        }
        return idx
    }

    checkTime = ()=>{
        if ( this.state.moving !== 0 ){
            this.setState(state=>{
                let moving = this.state.moving
                let obj = {moving}
                const timesup = ( new Date().getTime() > this.state.doneMoving)
               
                if ( moving > 0) {
                   // moving++
                    if ( timesup ){
                        obj.currentIndex = this.safe(state.covers, state.currentIndex+1)
                        obj.moving = 0
                        this.props.dispatch(actions.setPostcardFront(this.getListBasedOn(this.state.covers,obj.currentIndex)[2]))
                    }
                }
                if ( moving < 0) {
                   // moving--
                    if ( timesup ){
                        obj.currentIndex = this.safe(state.covers, state.currentIndex-1)
                        obj.moving = 0
                        this.props.dispatch(actions.setPostcardFront(this.getListBasedOn(this.state.covers, obj.currentIndex)[2]))
                    }
                }
                return {...obj}
            })
        }
        this.interval= requestAnimationFrame(this.checkTime)
    }

    componentWillMount(){
        this.setState({covers: new Array(this.props.preferences.PREF_POSTCARD_COUNT).fill(undefined).map((item,idx)=> './_postcards/front'+(idx+1)+'.png') })
        this.interval= requestAnimationFrame(this.checkTime)
    }
  
    onIncrement = (evt) =>{
        const delay = (new Date().getTime() + ANIM_DUR)
        this.setState({moving: 1, doneMoving: delay})
    }
    
    onDecrement = (evt) =>{
        const delay = (new Date().getTime() + ANIM_DUR)
        this.setState({moving: -1, doneMoving: delay})
    }
    
    componentWillUnmount(){
       cancelAnimationFrame(this.interval);
    }
    
    handleTouchStart = (evt)=>{
        evt.preventDefault()
        TOUCH.lastY = evt.type === "mousedown" ? evt.clientY : evt.nativeEvent.touches[0].clientY
        TOUCH.startTime = (new Date()).getTime()
    }
    
    handleTouchEnd = (evt)=>{
        evt.preventDefault()
        const DELAY = 500 //time within they must let go to "count"
        const MIN = 100 // height must move to "count"
        const thisY = evt.type === "mouseup" ? evt.clientY : evt.nativeEvent.touches[0].clientY
        const moved = thisY - TOUCH.lastY
        if ( (TOUCH.startTime + DELAY) > (new Date()).getTime()  &&
             Math.abs( moved ) > MIN){
            if ( moved > 0 ){
                this.onDecrement()
            }else{
                this.onIncrement()
            }
        }
    }

    onBlockScroll = (evt)=>{
       // console.log("block", evt)
        evt.preventDefault()
    }
    
    render(){
     
        const {currentIndex, moving, covers} = this.state
        const activeList = this.getListBasedOn(covers, currentIndex)
        const  h=180
        const offset = -180
        const top = offset + (moving > 0 ? -h : moving < 0 ? h : 0) 
        
        return (

            <div className="grid-container grid-y" 
            onWheel={this.onBlockScroll}
            onTouchStart={this.handleTouchStart}
            onTouchEnd={this.handleTouchEnd}
            style={{height:'100%'}} >
                    
                <div className="auto" onClick={this.onDecrement}><i className="big-arrow fi-arrow-up"></i></div>

                <div className="auto small-9 scrollcontainer shrink" style={{height: '540px'}}>

                    <div className={top === offset ? 'freezeit' : 'scrollit'} style={{top}} >
                    {activeList.map((color,idx)=>{
                        const className = idx === 2 && moving===0 ? 'highlight-thumb' : 'no-highlight-thumb'
                      return  <img  alt="" id={idx}  key={idx}
                                    className={className}  
                                    onClick={ idx===1 ? this.onDecrement : idx===3 ? this.onIncrement : null}
                                    src={activeList[idx]}/>
            
                    })}
                    </div>
                
                </div>

                <div className="auto small-1" onClick={this.onIncrement}><i className="big-arrow fi-arrow-down downarrow"></i></div>

            </div>
            
            
        )
  }
  
}

export default connect(Utils.mapStateToProps)(PostcardScroller)
