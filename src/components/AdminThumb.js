import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'
import actions from '../actions'

class AdminThumb extends React.Component{

    state = {playing: false}

    onClick = (evt)=>{
        this.props.dispatch(actions.clickVideoInRow(this.props.data.video))
    }

    render(){
        const { video, idx} = this.props.data;
        const {thumb} = video;
        const { selectedRowAdmin} = this.props;
        return (
            <div className="admin-video-thumb" onClick={this.onClick} >
            
            {   (idx === selectedRowAdmin && false) ? 
                    <video  className='admin-video' 
                        controls src={video}  
                        id="recording"></video>
            :         
                    <div onClick={this.onClick} className='admin-video'  >
                        <img alt="na" className="admin-video-thumb-imgs"
                        src={thumb} /> 
                    </div>
            }  
                   
            </div>
        )
    }
}


export default connect(Utils.mapStateToProps)(AdminThumb)