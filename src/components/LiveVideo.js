import React from 'react'
import Utils from '../services/utils'

import { connect } from 'react-redux'

class LiveVideo extends React.Component{

  state = {showPlayback:false, isHidden:false}

    promise = null
    constraintPromise  = null
    stream = null 
    mediaRecorder = null
    chunks = []
    blob = null
    options = {
      audioBitsPerSecond : 128000,
      videoBitsPerSecond : 4000000,
      mimeType: 'video/webm;codecs=vp9',
    }
    
    hide(){
      this.setState({isHidden:true})
    }
    show(){
      this.setState({isHidden:false})
    }
    getVideoElement(){
      console.log("returning..."+document.querySelector('#live'))
      return document.querySelector('#live')
    }
    pausePlayback(){
      if ( this.recording.src ) this.recording.pause()
    }
    record(){
        this.video.volume = 0
        this.chunks = []
        this.mediaRecorder.start()
        this.setState({showPlayback:false})
    }
    stop(){
      this.mediaRecorder.stop()
      this.setState({showPlayback:true})
    }
    getBin(){
      return this.blob
    }
    showLive(){
      this.pausePlayback()
      this.setState({showPlayback:false})
    }
    playrecording(){
      console.log( this.recording )
      this.recording.play()
      this.setState({showPlayback:true})
    }
    
    componentDidMount = () =>{

      if ( this.props.onRef ) {
        this.props.onRef(this)
      }
      
      this.video = document.getElementById('live')
      this.recording = document.getElementById('recording')

      let micid = this.props.micid
      let camid = this.props.camid 

      const w = this.props.info ? this.props.info.width : null;
      const h = this.props.info ? this.props.info.height : null;
      const width = w || (Utils.onDesktop() ? 640 : 1920)
      const height = h || (Utils.onDesktop() ? 480 : 1080)

      //start media
      let constraints = { audio:  {deviceId: micid},
                          video:  { frameRate: 30, width, height, deviceId: camid} }

      this.video.volume = 0
      
      this.promise = navigator.mediaDevices.getUserMedia(constraints)
      .then( (stream) =>{

          this.stream = stream
          /* use the stream */
          this.video.srcObject = stream
          this.video.play()

          const track = stream.getVideoTracks()[0];
       
          const constraints = {frameRate: 30, width, height}
        
          this.constraintPromise = track.applyConstraints(constraints)

          .then( ()=>{
          
            if (this.props.onReady) {
              this.props.onReady()
            }

            this.mediaRecorder = new MediaRecorder(this.stream, this.options);
          
            this.mediaRecorder.onstop = (e)  => {
            this.video.volume = 0
            
            this.blob = new Blob(this.chunks, { 'type' : this.mediaRecorder.mimeType})
            
            const theURL = window.URL.createObjectURL(this.blob)
            
            
            this.recording.src = theURL
            this.recording.controls = false;
              //sendVideo(blob)
          }
          this.mediaRecorder.ondataavailable =  (e) => {
            this.chunks.push(e.data);
          } 
        })
        .catch((err2)=>{
          console.log("error 2: " +  + err2.name + ": " + err2.message)
        })
    })
    .catch((err) => {
      
      console.log("safe error caught " + err.name + ": " + err.message)
    }) 
 }




  componentWillUnmount(){
    this.reset()
  }

  reset(){
   
    if ( this.video ){
      this.video.pause()
      this.video.srcObject=null
      delete this.video
    }
      
    if ( this.promise ) delete this.promise
    if ( this.mediaRecorder ) delete this.mediaRecorder

    if (this.stream) {
      if (this.stream.getVideoTracks) {
        // get video track to call stop on it
        var tracks = this.stream.getVideoTracks()
        if (tracks ) {
          tracks.forEach(track => {
            track.stop()})
        }
      }
      else if (this.stream.stop) {
        this.stream.stop();
      }
      delete this.stream;
    }
		
  }

  render(){
    const dotted = this.props.outlineColor === 'orange' ? 'dotted' : 'solid'
  
    return (
    
      <div style={{...this.props.style}}>
            <div hidden={this.state.isHidden} className='video-container' >
            
                <video  className="reversed-video" hidden={this.state.showPlayback} id="live" ></video>
                
                <video  className="regular-video" hidden={!this.state.showPlayback} id="recording"></video>
               
            </div>
            
            <div className="video-frame-container">
              
              <div className="video-frame" style={{outline: '10px '+dotted + ' ' + this.props.outlineColor }}  />
            
            </div>
      </div>
           
    )
  }
  
}


export default connect(Utils.mapStateToProps)(LiveVideo)