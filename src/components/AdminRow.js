import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'
import AdminThumb from './AdminThumb'
import MyButton from './MyButton'

class AdminRow extends React.Component{
 
    onClick = (evt)=>{
        if ( !this.props.pendingList.includes(this.props.data.video._id) ){
            this.props.dispatch(actions.setAdminRow(this.props.data.idx))
        }
    }
    onCheckmarkIt = (evt)=>{
        if(evt.target.checked){
            //add
            this.props.dispatch(actions.addSelectedVideos([this.props.data.video._id]))
        }else{
            //rem
            this.props.dispatch(actions.removeSelectedVideos([this.props.data.video._id]))
        }
    }
    onMark = (tracker, evt)=>{
        switch(tracker){
            case "take-out-of-trash": 
                this.props.dispatch(actions.undeleteVideos([this.props.data.video._id]))
               break;
            case "put-in-loop":
                this.props.dispatch(actions.putVideosInLoop([this.props.data.video._id], this.props.data.video.inloop!=='true'))
                break;
            case "put-in-archive":
                this.props.dispatch(actions.archiveVideos([this.props.data.video._id]))
                break;
            default:
                break;
        }

    }
    
    clickInfo = (evt)=>{
        console.log(evt.clientY / evt.target.getBoundingClientRect().height)
    }
    
    clickDownload = (evt)=>{
        this.props.dispatch(actions.trashVideos([this.props.data.video._id])) 
    }
    clickGetCVS = (evt)=>{
        Utils.downloadCSVFor([this.props.data.video],this.props.deletedVideo?"trash": "archive")
    }


    clickTrash = (evt)=>{
        if (this.props.deletedVideo){
            if( window.confirm("Are you sure you want to PERMANENTLY remove this video?") ){
                this.props.dispatch(actions.permanentlyDelete([this.props.data.video._id]))
            }else{
                //nothing
            }
        }else{
            if( window.confirm("Are you sure you want to trash this video?") ){
                this.props.dispatch(actions.trashVideos([this.props.data.video._id]))  
            }else{
                //nothing
            }
        }
    } 

    getVideoInfo = () =>{
        
        const {  guest_name,  email, date} = this.props.data.video

        return (
            <div className="video-info" >
                <div className="video-info-name"><span className="video-info-label">name: </span><span className="video-info-value">{unescape(guest_name)}</span></div>
                <div className="video-info-email"><span className="video-info-label">email: </span><span className="video-info-value">{unescape(email)}</span></div>
                <div className="video-info-date"><span className="video-info-value">{Utils.prettyFormatDate(date)}</span></div>
                
            </div>

        )
    }    
    
    render(){
        const {idx, video} = this.props.data
        const { _id, inloop, status} = video
        const {pendingList, selectedVideos} = this.props
        const disabled = pendingList.includes(_id)
        const color = this.props.selectedRowAdmin === idx ? 'darkgray' : 'gray'
     
       const isSelected = selectedVideos.includes(_id)
        return (
            <div id={'r_'+idx} className='grid-container container-left admin-row grid-x fade-changes'
                onClick={this.onClick} 
                style={{backgroundColor: color}} >

               <div className="auto shrink" style={{paddingLeft: '20px'}}>   
                        <input type="checkbox" 
                            checked={isSelected}
                            disabled={disabled} 
                            onClick={this.onCheckmarkIt} className="checkbox-med" id="inloop" value="in loop"/>
                   
               </div>

                <div className="small-2 grid-container" title="Review">
                    <AdminThumb  data={this.props.data}/>
                </div>
                <div className="small-4 grid-container" onClick={this.clickInfo} >
                            {this.getVideoInfo()}
                           
                </div>
                { this.props.deletedVideo ? 
                    <div className="auto grid-x cell container-left">
                        <MyButton 
                                className="x"
                                size="small"
                                disabled={disabled}
                                label="Take out of Trash"
                                id='take-out-of-trash'
                                click={this.onMark}/>
                    </div>
                    :
                    
                    <div className="auto grid-x cell container-left" >

                            <MyButton 
                                        className="x"
                                        size="small"
                                    disabled={disabled}
                                    style={{display: (status==="new") ? 'inherit' : 'none'}}
                                    label="Archive"
                                    id='put-in-archive'
                                    click={this.onMark}/>
                                    
                                <div style={{width: '20px'}}/>

                                <MyButton 
                                    className="x"
                                    size="small"
                                    disabled={disabled}
                                    label={status==="new" ? "Use in Loop" : inloop==='true' ? "Take out of Loop" : "Use in Loop"}
                                    id='put-in-loop'
                                    click={this.onMark}/>

                                    <div style={{width: '20px'}}/>

                                
                    </div>
                }

               
               
                <div className="cell shrink">
                    <div className=" trash-container" >
                        <div className={"auto " + (Utils.isKioWare() ? "hidden-display" : " ")}>
                            <a title="Download video" href={video.video} download>
                                <span className="auto fi-download large trash "/>
                            </a>
                        </div>
                        <div title="Get CSV data" onClick={this.clickGetCVS} 
                                className={"auto fi-graph-trend large trash " + (Utils.isKioWare() ? "hidden-display" : " ")} ></div>
            
                        <div title={this.props.deletedVideo?"Permanently Delete": "Trash"}  
                            onClick={this.clickTrash} 
                            className={"auto fi-trash "+ (this.props.deletedVideo ? "trash2" : "trash")}></div>
                    </div>
                </div>
            

            
            </div>

        )
    }
}

export default connect(Utils.mapStateToProps)(AdminRow)