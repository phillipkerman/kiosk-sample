import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import Parser from 'html-react-parser'

class MyButton extends React.Component{
    onOver = (evt) =>{
        evt.currentTarget.classList.add("rolloverbutton")
        evt.currentTarget.firstChild.classList.add('button-text-roll')
    }
    onOut = (evt) =>{
        evt.currentTarget.classList.remove("rolloverbutton")
        evt.currentTarget.firstChild.classList.remove('button-text-roll')
    }
    onClick = (evt)=>{
        if ( this.props.disabled ) return
        this.props.click(this.props.id, evt)
    }

    render(){
        const color = this.props.fill ? this.props.fill : ""
        
        const textClassName = this.props.size==="big" ? 'button-text-big unselectable': this.props.size==="small" ? 'button-text-small unselectable' : 'button-text unselectable'
        const {disabled} = this.props
        
        return (
            <div  className={'button bordered '+this.props.className}
            disabled={disabled}
            style={{...this.props.style, backgroundColor: color}}
                onTouchStart={this.onOver} 
                onTouchEnd={this.onOut} 
                onMouseOver={this.onOver} 
                onMouseOut={this.onOut} 
                onClick={this.onClick}>
                    <span onMouseOver={evt=>null} onMouseOut={evt=>null}  className={textClassName}>{Parser(this.props.label)}</span>
            </div>
        )
    }
}


export default connect(Utils.mapStateToProps)(MyButton)