import React from 'react'
import { connect } from 'react-redux'
import Utils from '../services/utils'
import actions from '../actions'
import MyButton from './MyButton'
import AdminRow from './AdminRow'
import VideoPlayerOverlay from './VideoPlayerOverlay'
import InputNumber from './InputNumber'
import InputText from './InputText'
import CamMicSelect from './CamMicSelect'

import {PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN,
        PREF_COUNTDOWN_BEFORE_VIDEO_STARTS,
        PREF_MAX_VIDEO_DURATION,
        PREF_START_WARNING_AT,
        PREF_INACTIVITY_TIMEOUT,
        from_email,
        from_password,
        from_service,
        PREF_POSTCARD_COUNT,
    } from '../actions'

class ScreenAdmin extends React.Component{


onNav = (screen) =>{
    this.props.dispatch(actions.setScreen(screen))
}

interval = -1
state = {loading:false,
            error: null,
            isDirty:false,
            pendingChanges: {}}

onRefresh = () =>{
    this.props.dispatch(actions.refreshVideoList()) 
}

componentWillUnmount(){
    if ( this.interval ){
        clearInterval(this.interval)
    }
}
componentDidUpdate(){
   const el = document.getElementById('r_'+ this.props.selectedRowAdmin)
   const container = document.getElementById('admin-list')
  
   if ( el ){  
       const bot = el.getBoundingClientRect().top + el.getBoundingClientRect().height
       const edge = container.getBoundingClientRect().height + container.getBoundingClientRect().y
   }
}

componentDidMount(){
    this.setState({isDirty: false, pendingChanges:{}})
    this.props.dispatch(actions.setAdminRow(-1))

      this.onRefresh()
      this.interval = setInterval(()=>{
        this.onRefresh()
      },3000)
      
  }
  doFilter = (evt)=>{
    this.props.dispatch(actions.setFilterTab(evt.target.id))
    this.setState({isDirty: false, pendingChanges:{}})
    document.getElementById('allcb').checked=false
  }
  
  matchingVideos = ()=>{
    switch ( this.props.filterTab ){
        case "filter_new":
            return this.props.videos.filter(this.justnew)
        case "filter_loop":
            return this.props.videos.filter(this.inloop)
        case "filter_all":
            return this.props.videos.filter(this.notnew)
        case "filter_trash":
            return this.props.deletedvideos
        default:
            return [];
    } 
  }
  onMarkMany = (tracker)=>{
    switch(tracker){
        case "put-in-loop":
            this.props.dispatch(actions.putVideosInLoop(this.props.selectedVideos, true))
            break;

        case "take-out-of-loop":
            this.props.dispatch(actions.putVideosInLoop(this.props.selectedVideos, false))
            break;

        case "put-in-archive":
            this.props.dispatch(actions.archiveVideos(this.props.selectedVideos))
            break;
            
        case "take-out-of-trash":
            this.props.dispatch(actions.undeleteVideos(this.props.selectedVideos))
            break;

        case "permanently_delete":
            //confirm then:
            const n2 = this.props.selectedVideos.length
            if( window.confirm("Are you sure want to PERMANENTLY delete " +( n2 > 1 ? " these videos?" : "this video?") ) ){
                this.props.dispatch(actions.permanentlyDelete(this.props.selectedVideos))
            }else{
                //nothing
            }
            break;

        case "trash-videos":
            //confirm then:
            const n = this.props.selectedVideos.length
            if( window.confirm("Are you sure want to trash " +( n > 1 ? "the selected videos?" : "the selected video")) ){
                this.props.dispatch(actions.trashVideos(this.props.selectedVideos))  
            }else{
                //nothing
            }
            break;
            
        case "export":
        
            Utils.downloadCSVFor(this.matchingVideos(), this.props.filterTab==="filter_trash" ? "trash" : "archive")
            break;
        
        default:
            break;
    }
  }
  getPrompt = (n)=>{
    const buttonArchive = <MyButton 
                            size='small'
                            label={"Archive " + n + " video" + (n > 1 ? "s" : "")}
                            id='put-in-archive'
                            click={this.onMarkMany}/>
    const buttonLoopIn = <MyButton 
                            size='small'
                            label={"Move " + n + " video" + (n>1 ? "s": "") + " into loop"}
                            id='put-in-loop'
                            click={this.onMarkMany}/>
                        
    const buttonLoopOut = <MyButton 
                            size='small'
                            label={"Take " + n + " video" + (n>1 ? "s": "") + " out of loop"}
                            id='take-out-of-loop'
                            click={this.onMarkMany}/>
     
    const buttonTrash = <MyButton 
                            size='small'
                            fill={'darkred'}
                            label={"Trash " + n + " video" + (n>1 ? "s": "")}
                            id='trash-videos'
                            click={this.onMarkMany}/>
                            
    const buttonTrashOut = <MyButton 
                            size='small'
                            fill={'green'}
                            label={"Un-Trash " + n + " video" + (n>1 ? "s": "")}
                            id='take-out-of-trash'
                            click={this.onMarkMany}/>
                            
    const buttonPermanentlyDelete = <MyButton 
                            size='small'
                            label={"Permanently delete " + n + " video" + (n>1 ? "s": "")}
                            id='permanently_delete'
                            click={this.onMarkMany}/>


    const buttonExport = <MyButton 
                            disabled={ Utils.isKioWare() }
                            size='small'
                            fill={'black'}
                            label={"Export CSV Data for " + n + " video" + (n>1 ? "s": "")}
                            id='export'
                            click={this.onMarkMany}/>

    switch ( this.props.filterTab ){
        case "filter_new":
            return <div className="buttons-in-bar grid-x" >
                        <div className="cell shrink">{buttonArchive}</div>
                        <div className="cell auto" style={{width: '20px'}}/>
                        <div className="cell shrink">{buttonLoopIn}</div>
                        <div className="cell auto" style={{width: '20px'}}/>
                        <div className="cell shrink">{buttonTrash}</div>
                        <div className="cell auto" style={{width: '20px'}}/>
                        <div className="cell shrink">{buttonExport}</div>
                    </div>

        case "filter_loop":
            return  <div className="buttons-in-bar grid-x">
                        <div >{buttonLoopOut}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonExport}</div>
                    </div>


        case "filter_all":
            return <div className="buttons-in-bar grid-x">
                        <div >{buttonLoopIn}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonLoopOut}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonTrash}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonExport}</div>
                    </div>

        case "filter_trash":
            return <div className="buttons-in-bar grid-x">
                        <div >{buttonTrashOut}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonExport}</div>
                        <div style={{width: '20px'}}/>
                        <div >{buttonPermanentlyDelete}</div>
                        
                    </div>

        default:
            return "nonthing";
    } 
  }
  onClickAll = (evt)=>{
    const list=this.matchingVideos().map(video=>video._id)
    if (evt.target.checked){
        if ( list.length !== this.props.selectedVideos.length ){
            this.props.dispatch(actions.addSelectedVideos(list))
        }
    }else{
        this.props.dispatch(actions.removeSelectedVideos(list))
    }
  }

  justnew = (video)=>(video.status === "new")
  inloop = (video) =>(video.inloop==='true')
  notnew = (video) =>(video.status !== 'new')

 
  onChangePref = (info)=>{
    const obj = {[info.pref]: info.value}
   
    this.setState(state => ({isDirty: true,
                                pendingChanges: {...state.pendingChanges, ...obj}}))
  }
  onSaveChanges = ()=>{
    this.props.dispatch(actions.setPreferences(this.state.pendingChanges))
    this.setState({isDirty:false})
  }

  showSelect = ()=>{
      return this.props.filterTab === "filter_new" ||
      this.props.filterTab === "filter_loop" ||
      this.props.filterTab === "filter_all" ||
      this.props.filterTab === "filter_trash"
  }
  render(  ){
    const howManySelected = this.props.selectedVideos.length

    const {camid, micid} = this.props.preferences

    const toBlurOrNot = this.props.videoPlayerVideo ? " blurit" : " "

    const pcPrefsCol1 = ['pc_title', 'pc_button_add_snapshot', 'pc_button_on_snapshot',
    'pc_label_friends_name','pc_default_friends_name', 'pc_label_your_name', 'pc_default_your_name',
    'pc_label_friends_email']
    const pcPrefsCol2 = ['pc_label_friends_email_warning','pc_default_friends_email','pc_button_submit',
    'pc_button_back','pc_button_resnap','pc_prompt_smile','pc_button_cancel','pc_button_retake']
    const pcPrefsCol3 = ['pc_button_accept','pc_dialog_busy','pc_dialog_complete','pc_email_subject']

    const videoPrefs1 = ['v_title','v_record','v_re_record', 'v_cancel', 'v_stop', 'v_preview', 'v_label_name', 'v_prompt_name']
    const videoPrefs2 = ['v_label_email','v_label_email_warning','v_prompt_email','v_submit', 'v_back', 'v_countdown','v_countdown_end','v_timesup']
    const videoPrefs3 = ['v_dialog_busy','v_dialog_complete']

    const adminPrefs = this.props.superuser ? ['sa_title', 'sa_button_video', 'sa_button_postcard'] : []
   

    return (
    
    
      <div className='fullscreen grid-y' >

        <div className={"auto container grid-x"+toBlurOrNot} >
            
            <div className="small-4 grid-container grid-x">
                <MyButton   click={this.onNav} 
                            label={ Utils.isKioWare() ? "Kiosk Mode" : "See in Context"}
                            id="Attract"/>
                              
                           
               
            </div>
            <div className="small-7">
                <h1 className="specialfont" >Admin</h1>
            </div>
        </div>
        
        <div className={'auto tab-bar small-1 grid-x  grid-container container '+toBlurOrNot} 
            style={{backgroundColor:'lightblue', width: '100%'}}>
           
            <div onClick={this.doFilter} id="filter_new" 
                className="auto cell tab tab-button"
                style={{backgroundColor:this.props.filterTab==='filter_new'?'gray':null}}>
                    New ({this.props.videos.filter(this.justnew).length})
            </div>
            <div onClick={this.doFilter} id="filter_loop"
                    className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_loop'?'gray':null}}>
                        Loop ({this.props.videos.filter(this.inloop).length})
            </div>
            <div onClick={this.doFilter} id="filter_all" 
                className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_all'?'gray':null}}>
                    Archived ({this.props.videos.filter(this.notnew).length})
            </div>
            
            <div onClick={this.doFilter} id="filter_trash" 
                className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_trash'?'gray':null}}>
                    Trash ({this.props.deletedvideos.length})
            </div>

            <div onClick={this.doFilter} id="filter_prefs" 
                className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_prefs'?'gray':null}}>
                   Prefs
            </div>

            {  this.props.superuser ? 
            <div onClick={this.doFilter} id="filter_v" 
                className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_v'?'gray':null}}>
                   v_ labels
            </div>
            : null}
             {  this.props.superuser ? 
                <div onClick={this.doFilter} id="filter_pc" 
                    className="auto cell tab tab-button"  style={{backgroundColor:this.props.filterTab==='filter_pc'?'gray':null}}>
                    pc_ labels
                </div>
            : null}
            
        </div>

        {  (this.state.error) ? 
                
            (<h1 className='small-8'>{this.state.error}</h1>) : //else...
           

            (this.state.loading===true)               ? 
                
                (<h2 className='small-9 specialfont'>Loading...</h2>)         :

                  (
                    (this.props.filterTab === "filter_prefs") ?
                    
                    
                    <div className='small-9 grid-x container' >
                        <div className="cell auto container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                       
                            <div style={{height: '20px'}}></div>
                          
                          
                                <div className="auto "> 
                                    <CamMicSelect allowed={Utils.isKioWare()} type="camid"  style={{width:'100%'}}
                                    onChange={this.onChangePref} inits={{camid}} {...this.props}/>
                                </div>
                                <div className="auto ">
                                    <CamMicSelect allowed={Utils.isKioWare()} type="micid"  style={{width:'100%'}}
                                    onChange={this.onChangePref} inits={{micid}} {...this.props}/>
                                </div>
                           
                        
                            <div className="auto "> 
                                <InputText 
                                        onChange={this.onChangePref}
                                        label="email username"  style={{width:'100%'}}
                                        pref={from_email} > 
                                </InputText>
                            </div>
                            <div className="auto ">
                                <InputText 
                                        password={true}
                                        onChange={this.onChangePref}
                                        className=" auto"  style={{width:'100%'}}
                                        label="email password"
                                        pref={from_password} > 
                                </InputText>
                            </div>
                            <div className="auto ">
                                <InputText 
                                        onChange={this.onChangePref}
                                        className=" auto"  style={{width:'100%'}}
                                        label="transport service (gmail)"
                                        pref={from_service} > 
                                </InputText>
                            </div>
                           


                        </div>

                        <div className="cell auto container-top-left grid-y" 
                        style={{height: '100%', backgroundColor: 'gray'}} >
                          <div style={{height: '20px'}}></div>
                            
                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="countdown before snapshot is snapped (in seconds)"
                                        pref={PREF_COUNTDOWN_BEFORE_SNAP_IS_TAKEN} > 
                            </InputNumber>
                                         
                                         
                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="countdown before video starts recording (in seconds)"
                                        pref={PREF_COUNTDOWN_BEFORE_VIDEO_STARTS} > 
                            </InputNumber>

                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="max video duration (in seconds)"
                                        pref={PREF_MAX_VIDEO_DURATION} > 
                            </InputNumber>

                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="seconds from end to show 'time remaining' countdown"
                                        pref={PREF_START_WARNING_AT} > 
                            </InputNumber>

                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="Kiosk Timeout (in seconds)"
                                        pref={PREF_INACTIVITY_TIMEOUT} > 
                            </InputNumber>

                            <InputNumber
                                        onChange={this.onChangePref} 
                                        className="cell auto" style={{width:'100%'}}
                                        label="total number of 'front<n>.png' images in c:\kiosk\build\_postcards"
                                        pref={PREF_POSTCARD_COUNT} > 
                            </InputNumber>


                            
                        </div>

                        <div className="cell auto container-top-left grid-y"  
                                style={{height: '100%', backgroundColor: 'gray'}} >
                            
                             <div style={{height: '20px'}}></div>
                            
                                { adminPrefs.map(str=>{
                                    return (
                                        <InputText 
                                                onChange={this.onChangePref}
                                                className=" auto"  style={{width:'100%'}}
                                                label={str}
                                                key={str}
                                                pref={str} > 
                                        </InputText>
                                       
                                        
                                        )
                                })
                                }

                            
                            </div>
                            <div className="cell small-5 container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                                <div style={{height: '40px'}}></div>
                            {this.props.superuser ? 
                              <img className="guideimage" alt="" src={'./guides/guide_attract.png'}/>
                              : null}

                              <div style={{height: '20px'}}></div>
                            
                              <MyButton   click={this.onSaveChanges} 
                                    disabled={!this.state.isDirty}
                                    label="save changes"
                                    id="save"/>
                            </div>


                       
                    </div>

                    
                    :
                    ( (this.props.filterTab === "filter_pc") ? 
                        <div className='small-9 grid-x container'>
                        <div className="cell auto container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                            
                            <div style={{height: '20px'}}></div>
                            
                        { pcPrefsCol1.map(str=>{
                                return (
                                    <InputText 
                                            onChange={this.onChangePref}
                                            className=" auto"  style={{width:'100%'}}
                                            label={str}
                                            key={str}
                                            pref={str} > 
                                    </InputText>
                                  
                                    
                                    )
                            })}
                        </div>

                        <div className="cell auto container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                            
                            <div style={{height: '20px'}}></div>
                            

                        { pcPrefsCol2.map(str=>{
                                return (
                                    <InputText 
                                            onChange={this.onChangePref}
                                            className=" auto"  style={{width:'100%'}}
                                            label={str}
                                            key={str}
                                            pref={str} > 
                                    </InputText>
                                   
                                    )
                            })}
                        </div>
                        <div className="cell auto container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                            
                            <div style={{height: '20px'}}></div>
                            

                            { pcPrefsCol3.map(str=>{
                                    return (
                                        <InputText 
                                                onChange={this.onChangePref}
                                                className=" auto"  style={{width:'100%'}}
                                                label={str}
                                                key={str}
                                                pref={str} > 
                                        </InputText>
                                       
                                        )
                                })}
                        </div>
                        <div className="cell small-6 container-top-left grid-y"  
                            style={{height: '100%', backgroundColor: 'gray'}} >
                            
                              <img className="guideimage" alt="" src={'./guides/guide_postcard.png'}/>
                              <div style={{height: '20px'}}></div>
                            
                              <MyButton   click={this.onSaveChanges} 
                                    disabled={!this.state.isDirty}
                                    label="save changes"
                                    id="save"/>
                        </div>

                        </div>
                    :
                    ( (this.props.filterTab === "filter_v") ? 
                        <div className='small-9 grid-x container'>
                            
                            <div className="cell auto container-top-left grid-y"  
                                style={{height: '100%', backgroundColor: 'gray'}} >
                             <div style={{height: '20px'}}></div>
                            
                                { videoPrefs1.map(str=>{
                                    return (
                                        <InputText 
                                                onChange={this.onChangePref}
                                                className=" auto"  style={{width:'100%'}}
                                                label={str}
                                                key={str}
                                                pref={str} > 
                                        </InputText>

                                        
                                        )
                                })
                                }
                            
                            </div>
                            <div className="cell auto container-top-left grid-y"  
                                style={{height: '100%', backgroundColor: 'gray'}} >
                             <div style={{height: '20px'}}></div>
                            
                                { videoPrefs2.map(str=>{
                                    return (
                                        <InputText 
                                                onChange={this.onChangePref}
                                                className=" auto"  style={{width:'100%'}}
                                                label={str}
                                                key={str}
                                                pref={str} > 
                                        </InputText>
                                      
                                        
                                        )
                                })
                                }
                            
                            </div>

                            <div className="cell auto container-top-left grid-y"  
                                style={{height: '100%', backgroundColor: 'gray'}} >
                             <div style={{height: '20px'}}></div>
                            
                                { videoPrefs3.map(str=>{
                                    return (
                                        <InputText 
                                                onChange={this.onChangePref}
                                                className=" auto"  style={{width:'100%'}}
                                                label={str}
                                                key={str}
                                                pref={str} > 
                                        </InputText>
                                    
                                        
                                        )
                                })
                                }

                                <div className="cell small-6"  >
                                    <InputText 
                                            tall={true}
                                            onChange={this.onChangePref}
                                            className=" auto"  style={{width:'100%'}}
                                            label='v_waiver'
                                            key='v_waiver'
                                            pref='v_waiver' > 
                                    </InputText>
                                </div>

                                

                            
                            </div>
                            <div className="cell small-7 container-top-left grid-y"  
                                style={{height: '100%', backgroundColor: 'gray'}} >
                                <div style={{height: '40px'}}></div>
                                <img className="guideimage" alt="" src={'./guides/guide_video.png'}/>
                                <div style={{height: '20px'}}></div>
                                <MyButton   click={this.onSaveChanges} 
                                    disabled={!this.state.isDirty}
                                    label="save changes"
                                    id="save"/>
                            </div>






                        </div>
                    
                    :
                        <div className={'small-9  grid-x container'+toBlurOrNot} style={{width:'100%'}}>
                        

                            <div className="cell auto container-top admin-list-container"
                                style={{ visibility: (this.props.filterTab !== "filter_new" &&
                                                    this.props.filterTab !== "filter_loop" &&
                                                    this.props.filterTab !== "filter_all" &&
                                                    this.props.filterTab !== "filter_trash")
                                ? 'hidden' : null}}>
                                <div className="admin-list" id='admin-list'  >
                                    {this.matchingVideos().map((video,idx)=>(
                                        <AdminRow  deletedVideo={this.props.filterTab === "filter_trash"} 
                                        key={idx} data={{video, idx}} />
                                    ))}
                                </div>
                            </div>
                        </div>
                    )
                  )   
                
                )       
        }
 
        <div className={"grid-x small-1 shrink select-bar container"+toBlurOrNot}  
        style={{visibility: this.showSelect() ? null : 'hidden' }}>

                <div className="cell small-2 container-left">
                
                        <input type="checkbox"
                            disabled={this.matchingVideos().length===0} 
                            onClick={this.onClickAll} 
                            className="checkbox-med" 
                            id="allcb" value="in loop"/>
                        <label className="admin-checkboxtext" htmlFor={'allcb'}>Select All</label>
                    
                </div>
            
            <div className="small-10 grid-container">
                    { howManySelected===0 ? 
                    null :
                    this.getPrompt(howManySelected)
                    
                }
                </div>
                
        </div>
   
  
        {this.props.videoPlayerVideo ? 
            <VideoPlayerOverlay {...this.state} video={this.props.videoPlayerVideo}/> :
        null }
     
        </div>
    
        
    )
  }
  
}

export default connect(Utils.mapStateToProps)(ScreenAdmin)