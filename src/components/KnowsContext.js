import React from 'react'
import {Context} from '../contexts/Context'

class KnowsContext extends React.Component {
 
  render() {
    return (
      <Context.Consumer>{ global => (
          <h1 onClick={global.func}>{global.prop}</h1>
      )}
      </Context.Consumer>
    )
  }
}
export default KnowsContext
