import React from 'react'
import { connect } from 'react-redux'

import Utils from '../services/utils'
import actions from '../actions'

class  DialogOverlay extends React.Component{
 
    state = {interval: -1}
    
    componentWillMount(){
       const interval = window.setInterval(()=>{
         this.props.dispatch(actions.dialogGettingStale())
       }, 1000)
       this.setState({interval})
    }

    componentWillUnmount(){
        window.clearInterval(this.state.interval)
    }

    render(){
        return (
            <div className="dialog-overlay avoid-clicks" >
                <div className="dialog-overlay-container">
                    <div className="dialog-overlay-textbox">
                        <div className="dialog-overlay-details">
                            {this.props.dialogExpiresIn < 2 ? this.props.dialogConfirmationMessage : this.props.initialMessage}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(Utils.mapStateToProps)(DialogOverlay)