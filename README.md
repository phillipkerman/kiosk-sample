# Kiosk Excerpt
React+Redux+Express+Mongo app:

* in museum touch-screen kiosk
* users can create/send virtual postcards.
* users can record videos for possible inclusion in kiosk attract loop.
* Admin tool (run locally or remote) provides the museum with control over which videos are featured in the attract loop.
  
### Images
![Kiosk Installed in Museum](/support/onsite.jpg)

![Admin view](/support/admin-app.jpg)

![Postcard app](/support/postcard-app.jpg)

![Video Comment app](/support/video-comment-app.jpg)


